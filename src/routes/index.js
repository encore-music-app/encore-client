var debug = require('debug')('routes/index');
var express = require('express');
var app = express.Router();
var favicon = require('serve-favicon');
var path = require('path');
var fs = require('fs');

app.get('/robots.txt', function(req, res) {
	// Prevent 'good' robots being able to index us
	res.write('User-agent: *\nDisallow: /');
	res.end();
});

// Serve favicons
app.use(favicon(path.join(__dirname, '../../', 'dist', 'favicon.ico')));

app.use('/public', express.static(path.join(__dirname, '../../', 'dist')));

app.get('/public/*', function(req, res, next) {
	// Anything not found in /public will be thrown here
	res.status(404).end('Not Found');
});

function getRemotePath(req) {
	var match = req.originalUrl.slice(1).match(/[\/]+/g);
	var webdir = (match ? Array(match.length).fill('../').join('').slice(0, -1) : '.');

	return webdir;
}

app.get('/manifest.json', function(req, res, next) {
	try {
		var remote = getRemotePath(req);

		var file = path.resolve(`${__dirname}/../manifest.json`);
		var manifest = fs.readFileSync(file).toString('utf-8');
		manifest = manifest.replace(/\${__rootdir}/g, remote);
		res.end(manifest);
	} catch (e) {
		res.status(500).end('Sorry, the server is unavailable at this time.');
		console.error(`Error, unable to access ${path}! - ${e}`);
	}
});

// Serve default requests to index.html
app.get('*', function(req, res, next) {
	try {
		var remote = getRemotePath(req);

		var filepath = path.resolve(`${__dirname}/../../dist/index.htm`);
		var index = fs.readFileSync(filepath)
		.toString('utf-8')
		.replace(/\${__rootdir}/g, remote);

		res.end(index);
	} catch (e) {
		res.status(500).end('Sorry, the server is unavailable at this time.');
		console.error(`Error, unable to access ${__dirname}/../index.html! - ${e}`);
	}
});

// Setup basic 404 error handling
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

module.exports = app;