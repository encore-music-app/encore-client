/**
* Artists Template
*/
(new function Artists() {
    this.compile = function(data) {
        var tag = template.tag;
        //var tagRaw = template.tagRaw;

        if (!(data.artist || data.artists)) {
            return false;
        }

        if (data.artist) {
            var artist = data.artist;

            var uri = `${__rootdir}/artist/${artist.uri}`;
            return tag('.artist.showcase', [
                tag('div > img', {src: artist.cover}),
                tag('div', [
                    tag('h1 a', {'data-uri': uri}, artist.name),
                    tag('.controls > a.button', {'data-uri': uri, 'data-action': 'play'}, 'Play Artist')
                ])
            ]);
        }

        var icons = data.artists.map(function(artist) {
            return tag('div', {class: 'artist'}, [
                tag('div > a', {'data-uri': `${__rootdir}/artists/${artist.uri}`},
                    tag('img', {src: `${__rootdir}/public/images/avatar-light.png`})
                ),
                tag('p > b > a', {'data-uri': `${__rootdir}/artists/${artist.uri}`}, artist.name),
                tag('p', null, '(' + artist.album_count + ' Album' + (artist.album_count > 1 ? 's' : '') + ')')
            ]);
        });

        var result = '';
        if (!data.opts.append) {
            result = tag('h1.title', data.opts.title) + tag('.cover-icon-grid', icons)
        } else {
            result = icons.join('');
        }

        if (data.opts.view_more && status.page_count > status.page) {
            result += '<br><div style="text-align: center">' +
            tag('a', {class: 'button', 'data-uri': data.opts.view_more}, 'View More');
        }

        return result;
    };

    this.build = function(context, opts) {
        opts = Object.assign({}, opts || {});
        opts.params = opts.params || {};

        var data = (context && context.isOK() ? context.getData() : []);

        template.current.collection = context;
        template.current.template = this;

        if (opts.params.id && data) {
            data.cover = `${__rootdir}/public/images/avatar-light.png`;
            var content = this.compile({
                artist: data,
                opts: opts,
                context: context
            });

            return models.artists.getAlbums({artist: data.uri, limit: 5})
            .then((artistdata) => template.albums.build(artistdata, {
                text: true,
                title: 'Albums',
                view_more: `${__rootdir}/albums/?artist=${data.uri}`
            }))
            .then((albumlist) => content += albumlist)
            .then(() => models.artists.getTracks({artist: data.uri, limit: 5}))
            .then((trackdata) => template.tracks.build(trackdata, {
                text: true,
                title: 'Tracks',
                class: 'tracks',
                view_more: `${__rootdir}/tracks/?artist=${data.uri}`
            }))
            .then((tracklist) => {
                content += tracklist;

                if (opts.text) {
                    return content;
                }

                template.body.setTitle(data.name);
                return template.body.build({view: content});
            });
        } else if (opts.params.id && !data) {
            return template.error.build({view: true, title: 'Something has gone wrong :/', message: 'Artist doesn\'t exist'});
        } else if (!opts.params.id && data.length > 0) {
            opts.title = opts.title || 'Artists';
            var content = this.compile({
                artists: data,
                opts: opts,
                context: context
            });
            delete data;
            context.clearData();

            if (opts.append) {
                $('.view > .cover-icon-grid').append(content);
                template.body.redetect();
            }

            if (opts.text || opts.append) {
                return Promise.resolve(content);
            }

            template.body.setTitle(opts.title);
            return template.body.build({view: content});

        } else if (!opts.params.id && data.length == 0) {
            if (!(opts.append || opts.text)) {
                return template.error.build({view: true, title: opts.title, message: 'No artists currently exist'});
            } else {
                content = `<h1>${opts.title}</h1><div class="cover-icon-grid"><h2>No results</h2></div>`;
                return (opts.text || opts.append ? content : template.body.build({view: content}));
            }
        }
    };
});