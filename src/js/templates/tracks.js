/**
* Tracks Template
*/
(new function Tracks() {
    this.compile = function(data) {
        var tag = template.tag;
        var tagRaw = template.tagRaw;

        if (!(data.track || data.tracks)) {
            return false;
        }

        if (data.track) {
            var track = data.track;

            var artists = track.artists.map(function(artist) {
                return tag('a', {'data-uri': `${__rootdir}/artists/${artist.uri}`}, artist.name)
            }).join(',&nbsp;');

            var uri = `${__rootdir}/tracks/${track.uri}`;
            return tag('.fullscreenflex.item > .track.showcase', [
                tag('div > img', {src: track.cover}),
                tag('div', [
                    tag('h1 a', {'data-uri': uri}, track.name),
                    tag('h3 a', {'data-uri': `${__rootdir}/albums/${track.album.uri}`}, track.album.name),
                    tag('span', artists),
                    tag('.controls > a.button', {'data-uri': uri, 'data-action': 'play'}, 'Play Track')
                ])
            ]);
        }

        var list = data.tracks.map(function(track) {
            var artists = track.artists.map(function(artist) {
                return tag('a', {'data-uri': `${__rootdir}/artists/${artist.uri}`}, artist.name)
            }).join(',&nbsp;');

            return tagRaw('tr', null,
            tagRaw('td', null, tag('a', {'data-uri': `${__rootdir}/tracks/${track.uri}`, 'data-action': 'play'}, track.name)) +
            tagRaw('td', null, tag('a', {'data-uri': `${__rootdir}/albums/${track.album.uri}`}, track.album.name)) +
            tagRaw('td', null, artists) +
            '<td><span>' + track.length + '</span></td>'
            );
        });

        var result = '';
        if (!data.opts.append) {
            result = '<caption><h1 class="title">' + data.opts.title + '</h1></caption>' +
            '<thead><tr>' +
            '<th>Track Name</th>' +
            '<th>Album</th>' +
            '<th>Artists</th>' +
            '<th>Time</th>' +
            '</tr></thead>' +
            '<tbody>' + list.join('') + '</tbody>';
            result = tag('table', {class: 'list' + (data.opts.class ? ' ' + data.opts.class : '')}, result);
        } else {
            result = list.join('');
        }

        if (data.opts.view_more && status.page_count > status.page) {
            result += '<br><div style="text-align: center">' +
            tag('a', {class: 'button', 'data-uri': data.opts.view_more}, 'View More');
        }

        return result;
    };

    this.detectActive = function() {
        var track = controller.getActiveContext();
        if (!track) {
            return;
        }
        $(".view table tbody tr").removeClass('active');
        $(".view table tbody tr a[data-uri*='tracks/']").each(function(i, e) {
            var match = e.dataset.uri.match(/tracks\/([\w\d]+)/);
            if (track.uri == match[1]) {
                $(e).parents('tr').last().addClass('active');
                return false;
            }
        });
    };

    const getTitle = function(opts) {
        if (!opts.title && opts.params && opts.params.query) {
            var params = opts.params.query
            if (params.search) {
                return Promise.resolve(`Search for '${params.search}'`);
            } else if (params.artist) {
                return models.artists.get({id: params.artist, no_collection: true})
                .then(function(res) {
                    if (res && res.data && res.data.name) {
                        return 'Tracks by ' + res.data.name;
                    } else {
                        return `Tracks by Artist (${params.artist})`;
                    }
                });
            } else if (params.album) {
                return models.albums.get({id: params.album, no_collection: true})
                .then(function(res) {
                    if (res && res.data && res.data.name) {
                        return 'Tracks by ' + res.data.name;
                    } else {
                        return `Tracks by Album (${params.album})`;
                    }
                });
            } else if (params.playlist) {
                return models.playlists.get({id: params.playlist, no_collection: true})
                .then(function(res) {
                    if (res && res.data && res.data.name) {
                        return 'Tracks from' + res.data.name;
                    } else {
                        return `Tracks from Playlist (${params.playlist})`;
                    }
                });
            }
            delete params;
        }
    };

    this.build = function(context, opts) {
        opts = Object.assign({}, opts || {});
        opts.params = opts.params || {};

        var data = (context && context.isOK() ? context.getData() : []);

        template.current.collection = context;
        template.current.template = this;

        if (opts.params.id && data) {
            data.cover = models.tracks.getCoverUrl({id: data.uri, size: 'medium'});
            var content = this.compile({track: data});
            //return template.quickRender('control/track', {track: track})
            //.then((content) => {
            if (opts.text) return Promise.resolve(content);
            template.body.setTitle(data.name);
            delete data;
            context.clearData();
            return template.body.build({view: content});
        } else if (opts.params.id && !data) {
            return template.error.build({view: true, title: 'Something has gone wrong :/', message: 'Track doesn\'t exist'});
        } else if (!opts.params.id && data.length > 0) {

            opts.title = opts.title || 'Tracks';
            opts.class = opts.class || 'tracks';

            // var compile_timer = Date.now();
            var promise;
            if (!opts.title) {
                promise = getTitle(opts);
            } else {
                promise = Promise.resolve(opts.title);
            }

            return promise.then(function(title) {
                opts.title = title;
                var content = template.tracks.compile({
                    opts: opts,
                    tracks: data,
                    context: context
                });

                // compile_timer = (Date.now() - compile_timer);
                // console.log(`Template render tracks (compile: ${compile_timer}ms)`);

                delete data;
                context.clearData();

                if (opts.append) {
                    $('.view > .list.tracks').eq(0).append(content);
                    content = null;
                    template.tracks.detectActive();
                }

                if (opts.text || opts.append) {
                    return Promise.resolve(content);
                }

                template.body.setTitle(opts.title);

                return template.body.build({view: content}).then(() => {
                    template.tracks.detectActive();
                });
            });
        } else if (!opts.params.id && data.length == 0) {
            if (!(opts.append || opts.text)) {
                return template.error.build({view: true, title: opts.title, message: 'No tracks available'});
            } else {
                content = `<h1>${opts.title}</h1><div class=".list"><h2>No results</h2></div>`;
                return (opts.text || opts.append ? Promise.resolve(content) : template.body.build({view: content}));
            }
        }

    };
}());