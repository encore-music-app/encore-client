/**
* Search Template
*/
(new function Search() {
    this.build = function(context, opts) {
        opts = Object.assign({}, opts || {});
        template.current.collection = context;
        template.current.template = this;

        if (context && context.isOK() && (searched = context.getData())) {

            var promises = [];
            (['artists', 'albums', 'tracks']).forEach(function(val) {
                if (searched[val] && searched[val].getData().length > 0) {
                    promises.push(template[val].build(searched[val], {
                        text: true,
                        view_more: `${__rootdir}/${val}/?search=${searched[val].getOpts().query.search}`
                    }));
                }
            });

            if (promises.length > 0) {
                return Promise.all(promises).then(function(built) {
                    var content = '<h1>Search for ' + opts.params.query + '</h1>' + built.join('');
                    return template.body.build({view: content});
                });
            } else {
                return template.error.build({view: true, title: 'Whoops!', message: 'The search returned no results :/'});
            }
        }
    };
}());