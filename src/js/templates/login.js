/**
* Login Template
*/
(new function Login() {
    this.compile = function(data) {
        var tag = template.tag;
        return tag('div', {class: 'dialog'}, [
            //tag('div', {class: 'shortcuts'}),
            tag('div', {class: 'inner'}, [
                tag('h1', {align: 'center', style: 'font-family: Raleway; font-weight: 100; font-size: 3em;'}, 'Encore'),
                tag('p', {align: 'center'}, tag('span', null, 'You must login to continue')),
                (function() {
                    if (data.error && data.error.message) {
                        return tag('div', {style: 'color: red; text-align: center'}, 'Error: ' + error.message.toLowerCase());
                    }
                }()),
                tag('form', {method: 'post'}, [
                    tag('input', {
                        name: 'username',
                        placeholder: 'Username',
                        value: data.username,
                        autocomplete: 'current-username'
                    }),
                    tag('input', {
                        name: 'password',
                        placeholder: 'Password',
                        value: data.password,
                        autocomplete: 'current-password',
                        type: 'password'
                    }),
                    tag('input', {
                        type: 'submit',
                        value: 'Login',
                    }),
                ])
                //tag('div', {class: 'action'})
            ])
        ])
    };

    this.build = function(opts) {
        opts = Object.assign({
            username: '',
            password: '',
            error: undefined
        }, opts || {});
        var loginform = this.compile(opts);
        $('body').empty().append(loginform);
        var self = this;
        $('.dialog form').on('submit', function(event) {
            event.preventDefault();
            var username = $('input[name=\'username\']').val() || '';
            var password = $('input[name=\'password\']').val() || '';
            if (username !== '' && password !== '') {
                models.users.authorise(username, password).then(function() {
                    window.location.reload();
                }).catch(function(e) {
                    self.build({error: e, username: username, password: password});
                })
            } else {
                self.build({error: {message: 'Username and password must not be blank!'}, username: username, password: password});
            }
        });
        return Promise.resolve();
    };
}());