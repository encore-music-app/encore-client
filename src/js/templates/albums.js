/**
* Albums Template
*/
(new function Albums() {
    this.compile = function(data) {
        var tag = template.tag;
        //var tagRaw = template.tagRaw;

        if (!(data.album || data.albums)) {
            return false;
        }

        if (data.album) {
            var album = data.album;

            var artists = album.artists.map(function(artist) {
                return tag('a', {'data-uri': `${__rootdir}/artists/${artist.uri}`}, artist.name)
            }).join(',&nbsp;');

            var uri = `${__rootdir}/albums/${album.uri}`;
            return tag('.album.showcase', [
                tag('div > img', {src: album.cover}),
                tag('div', [
                    tag('h1 a', {'data-uri': uri}, album.name),
                    tag('span', artists),
                    tag('.controls > a.button', {'data-uri': uri, 'data-action': 'play'}, 'Play Album')
                ])
            ]);
        }

        var icons = data.albums.map(function(album) {
            return tag('div', {class: 'album'}, [
                tag('div > a', {'data-uri': `${__rootdir}/albums/${album.uri}`},
                    tag('img', {src: models.albums.getCoverUrl({id: album.uri, size: 'small'})})
                ),
                tag('p > b > a', {'data-uri': `${__rootdir}/albums/${album.uri}`}, album.name),
                tag('p', null, (album.artists && album.artists.length > 0 ?
                    album.artists.map(function(artist) {
                        return tag('a', {'data-uri': `${__rootdir}/artists/${artist.uri}`}, artist.name)
                    }).join(',&nbsp;') : ''
                ))
            ]);
        });

        var result = '';
        if (!data.opts.append) {
            result = tag('h1.title', data.opts.title) + tag('.cover-icon-grid', icons);
        } else {
            result = icons.join('');
        }

        if (data.opts.view_more && status.page_count > status.page) {
            result += '<br><div style="text-align: center">' +
            tag('a', {class: 'button', 'data-uri': data.opts.view_more}, 'View More');
        }

        return result;
    };

    this.build = function(context, opts) {
        opts = Object.assign({}, opts || {});
        opts.params = opts.params || {};

        var data = (context && context.isOK() ? context.getData() : []);

        template.current.collection = context;
        template.current.template = this;

        if (opts.params.id && data) {
            data.cover = models.albums.getCoverUrl({id: data.uri, size: 'medium'});
            var content = this.compile({
                album: data,
                opts: opts,
                context: context
            });

            // Build list of tracks associated with the album
            return models.albums.getTracks({album: data.uri})
            .then((trackdata) => template.tracks.build(trackdata, {
                text: true,
                title: 'Tracks',
                class: 'tracks',
                view_more: `${__rootdir}/tracks/?album=${data.uri}`
            }))
            .then((tracklist) => {
                content += tracklist;
                if (opts.text) {
                    return content;
                }

                template.body.setTitle(data.name);
                return template.body.build({view: content});
            });
        } else if (opts.params.id && !data) {
            return template.error.build({view: true, title: 'Something has gone wrong :/', message: 'Album doesn\'t exist'});
        } else if (!opts.params.id && data.length > 0) {

            opts.title = opts.title || 'Albums';

            var content = this.compile({
                albums: data,
                opts: opts,
                context: context
            });
            delete data;
            context.clearData();

            if (opts.append) {
                $('.view > .cover-icon-grid').append(content);
                template.body.redetect();
            }

            if (opts.text || opts.append) {
                return Promise.resolve(content);
            }

            template.body.setTitle(opts.title);
            return template.body.build({view: content})

        } else if (!opts.params.id && data.length === 0) {
            if (!opts.append || !opts.text) {
                return template.error.build({view: true, title: opts.title, message: 'No albums currently exist'});
            } else {
                content = `<h1>${opts.title}</h1><div class="cover-icon-grid"><h2>No results</h2></div>`;
                return (opts.text || opts.append ? content : template.body.build({view: content}));
            }
        }

    };
});