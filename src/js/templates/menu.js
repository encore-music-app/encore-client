/**
* Menu Template
*/
(new (function Menu() {
    var self = this;
    this.compile = function(data) {
        var tag = template.tag;
        //var tagRaw = template.tagRaw;
        return tag('input#nav-main-menu', {type: 'checkbox'}) +
        tag('label.main.menu-toggle', {for: 'nav-main-menu'}) +
        tag('.menu-fixed-status', tag('span', null, template.body.getTitle() || 'Home')) +
        tag('nav.main.menu', [
            tag('form.search', {
                method: 'get',
                action: 'javascript:void();',
                'data-uri': '/search/:query'
            }, [
                tag('i.fa.fa-search'),
                tag('input', {name: 'query', placeholder: 'Search', value: data.search || ''})
            ]),
            data.dynamic
        ]);
    };

    this.playlists = (new function() {
        this.build = function() {
            return models.playlists.get({no_collection: true}).then(function(playlists) {
                var body = '';
                for(var i in playlists.data) {
                    var playlist = playlists.data[i];
                    body += `<li><a data-uri="${__rootdir}/playlists/${playlist.uri}"><span>${playlist.name}</span></a></li>`;
                }
                return Promise.resolve(`<ul>${body}</ul>`);
            });
        };
    });

    this.contextual = (new function() {
        var instance = null;

        this.isOpen = function() {
            return !!instance;
        };

        this.close = function() {
            if (instance) {
                instance.remove();
            }
            instance = null;
        }

        this.build = function(opts) {
            var opts = Object.assign({}, opts || {});

            if (this.isOpen()) {
                this.close();
            }

            if (!opts.items) {
                console.warn('No items were given. No contextual menu will be given');
                return;
            }

            var menu = '<div class="context-menu">' + parseMenu(opts.items) + '</div>';
            // Basic example:
            /*template.menu.build({
                items: [
                    {
                        name: 'Hello World',
                        icon: 'fa fa-heart',
                        link: "javascript:alert('World Hello')"
                    },
                ],
                css: {
                    left: 200px,
                    top: 200px
                }
            });*/
            if (opts.text) {
                return menu;
            }

            $('div.context-menu').remove();
            instance = $(menu);
            $('body').prepend(instance);
            if (opts.css.left) {
                opts.css.left = opts.css.left - ($('div.context-menu').innerWidth() / 2);
            }
            instance.css(opts.css);
            template.body.redetect();
            return Promise.resolve(instance);
        };

    });

    var isActiveMenuItem = function(uri) {
        var str = uri.replace(/\//g, '\\/');
        return !!window.location.pathname.match(`^(${str})`); //(`(${str}(\/$|$))`);
    };

    var parseMenu = function(menu) {
        var res = '';
        for (var i in menu) {
            var item = menu[i];
            if (!item || (item && typeof(item) === 'object' && Object.keys(item).length < 1)) {
                continue;
            }
            var icon = (item.icon ? `<i class='${item.icon}'></i>` : '');
            var link = (item.link ? `data-uri='${item.link}'` : '' /*'javascript:;'*/);
            var action = (item.action ? ` data-action='${item.action}'` : '');
            if (item.type && item.type === 'link') {
                link = (item.link ? `href='${item.link}'` : 'javascript:;');
                action = (item.action ? `onclick='${item.action}'` : 'javascript:;');;
            } else {
                // item.item === 'data'
            }
            var items = '';
            if (item.items && typeof(item.items) == 'string') {
                items = item.items;
            } else if (item.items && typeof(item.items) == 'object') {
                items = parseMenu(item.items);
            }
            var classes = item.class ? item.class.split(' ') : [];
            if (item.link && isActiveMenuItem(item.link)) {
                classes.push('active');
            }
            var classes = 'class=' + JSON.stringify(classes.join(' '));
            var label = (item.icon ? `<b>${item.label}</b>` : item.label);
            var listClasses = item.list_class;
            if (listClasses) {
                listClasses = `class="${listClasses}"`
            }
            res += `<li ${listClasses}><a ${classes} ${link} ${action}>${icon}<span>${label}</a>${items}</li>`;
        }

        return `<ul>${res}</ul>`;
    };

    this.refresh = function() {
        // Refresh the menu to make sure active item is changed, and that
        // playlists are refreshed
        return this.build().then(() => template.body.redetect());
    };

    this.build = function(opts) {
        opts = Object.assign({}, opts || {});

        return this.playlists.build()
        .then(function(playlists) {
            var menu = [
                /*(function() {
                    if (template.isMobile()) {
                        return */{
                            icon: 'fa fa-anchor',
                            label: 'Quick Actions',
                            list_class: 'mobile-only',
                            items: [
                                {label: 'Queue', link: `${__rootdir}/queue`, action: 'toggleUri'},
                                {label: 'Player', link: `${__rootdir}/player`}
                            ]
                        }/*;
                    }
                    return {};
                }())*/,
                {
                    icon: 'fa fa-globe',
                    label: 'Library',
                    items: [
                        {label: 'Tracks', link: `${__rootdir}/tracks`},
                        {label: 'Albums', link: `${__rootdir}/albums`},
                        {label: 'Artists', link: `${__rootdir}/artists`},
                        {label: 'Favourites', link: `${__rootdir}/favourites`}
                    ]
                },
                {
                    class: 'playlists',
                    icon: 'fa fa-inbox',
                    label: 'Playlists',
                    items: playlists
                },
                {
                    class: 'application',
                    icon: 'fa fa-cog',
                    label: 'Application',
                    items: [
                        {label: 'Settings', link: `${__rootdir}/settings`},
                        {label: 'Logout', link: `${__rootdir}/logout`},
                    ]
                }
            ];
            menu = /*self.menu.*/parseMenu(menu);

            var search = $('body > .app > nav.main.menu > form input[name=\'query\']');
            search = search.length > 0 ? search.val() : '';

            return self.compile(/*template.quickRender('control/menu', */{
                search: search,
                self: self,
                dynamic: menu
            });
        }).then(function(contents) {
            if (opts.text) {
                return contents;
            }

            $('body > .app > nav.main.menu, ' +
            'body > .app > .main.menu-toggle, ' +
            'body > .app > .menu-fixed-status, ' +
            'body > .app > input#nav-main-menu').empty().remove();
            $('body > .app').append(contents);
            //return Promise.resolve($('body > .app > nav.main.menu'));
        });
    };
})());