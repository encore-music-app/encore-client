/**
* Queue Template
*/
(new function Queue() {
    this.page = 0;
    var limit = 5000;

    this.compile = function(data) {
        var tag = template.tag;
        var tagRaw = template.tagRaw;

        var context = data.context;

        if (!(context && context.data && context.queue)) {
            return tag('.fullscreen.engraved.center.error > div', [
                tag('h1', data.opts.title),
                tag('h3', 'Queue is currently empty')
            ]);
        }

        this.page = this.page || context.queue.indexOf(context.active);
        var limit = context.queue.length;
        if (data.opts.limit) {
            limit = this.page + data.opts.limit;
        }
        var list = context.queue.slice(this.page, limit).map(function(item) {
            var track = context.data[item];
            var artists = track.artists.map(function(artist) {
                return tag('a', {'data-uri': `${__rootdir}/artists/${artist.uri}`}, artist.name)
            }).join(',&nbsp;');

            return tagRaw('tr', null,
            tagRaw('td', null, tag('a', {'data-uri': `${track.uri}`, 'data-action': 'play-queue-item'}, track.name)) +
            tagRaw('td', null, tag('a', {'data-uri': `${__rootdir}/albums/${track.album.uri}`}, track.album.name)) +
            tagRaw('td', null, artists) +
            '<td><span>' + track.length + '</span></td>'
            );
        });
        this.page += list.length;

        var result = '';
        if (!data.opts.append) {
            var title = data.opts.title + ' (' + (context.queue.length) + ')';
            result = tag('caption > h1.title', title) +
            '<thead><tr>' +
            '<th>Track Name</th>' +
            '<th>Album</th>' +
            '<th>Artists</th>' +
            '<th>Time</th>' +
            '</tr></thead>' +
            '<tbody>' + list.join('') + '</tbody>';
            result = tag('table', {class: 'list queue' + (data.opts.class ? ' ' + data.opts.class : '')}, result);
        } else {
            result = list.join('');
        }

        if (data.opts.view_more && status.page_count > status.page) {
            result += '<br><div style="text-align: center">' +
            tag('a', {class: 'button', 'data-uri': data.opts.view_more}, 'View More');
        }

        return result;
    };

    this.detectScroll = function() {
        var context = Object.assign({}, controller.getContext());
        if (template.queue.page < context.queue.length) {
            var view = $(this)[0];
            var viewTotal = view.scrollHeight - view.offsetHeight;
            var viewCurrent = view.scrollTop;
            //var status = template.current.collection.getStatus();
            if (viewCurrent / viewTotal > 0.75) {
                $('.view table tbody').append(template.queue.compile({
                    opts: {append: true, limit: 50},
                    context: context,
                }));
            }
        }
    };

    this.detectActive = function() {
        var track = controller.getActiveContext();
        if (!track) {
            return;
        }
        $(".view table tbody tr").removeClass('active');
        var active = $(".view table tbody tr a[data-uri='" + track.uri + "']");
        if (active) {
            $(active).parents('tr').last().addClass('active');
        }
    };

    function detectQueueUpdate(track) {
        template.queue.page = 0;
        if ($('.view > .list.queue').length > 0) {
            setTimeout(this.build.bind(this), 250);
        }
        controller.off('active-context-change', detectQueueUpdate);
        controller.off('context-update', detectQueueUpdate);
    };

    this.build = function(opts) {
        this.page = 0;
        opts = Object.assign({}, opts || {});

        var context = Object.assign({}, controller.getContext());

        template.current.collection = context.collection;
        template.current.template = this;

        opts.title = 'Queue'; // 'Up Next...', 'My Queue', 'Your Queue'
        opts.class = opts.class || '';
        opts.limit = limit;

        var content = this.compile({
            opts: opts,
            context: context
        });

        if (opts.append) {
            $('.view > .list.tracks').eq(0).append(content);
            content = null;
            template.tracks.detectActive();
        }

        if (opts.text || opts.append) {
            return Promise.resolve(content);
        }

        template.body.setTitle(opts.title);

        return template.body.build({view: content, infinite_scroll: false}).then(() => {
            this.detectActive();
            controller.on('active-context-change', detectQueueUpdate.bind(this));
            controller.on('context-update', detectQueueUpdate.bind(this));
            setTimeout(function() {
                $('.view').off('scroll').on('scroll', template.queue.detectScroll);
            }, 250);
        });

    };
}());