/**
* MiniPlayer Template
*/
(new (function MiniPlayer() {

    this.compile = function() {
        var tag = template.tag;

        return tag('.mini.player > .container', [
            tag('.progress > .tracker > .active'),
            tag('.playback', [
                tag('.fa.fa-random'),
                tag('.fa.fa-backward'),
                tag(`.fa.fa-chevron-up.mobile-only(data-uri="${__rootdir}/player", data-action="toggleUri")`),
                tag('.fa.fa-play'),
                tag('.fa.fa-forward'),
                tag('.fa.fa-repeat')
            ]),
            tag('.track > .information', [
                tag('.cover > .container', [
                    tag('img(src="", width=64, height=64)'),
                    tag('.love.active > .fa.fa-heart')
                ]),
                tag('div', [
                    tag('.name', 'Track Name'),
                    tag('.album', 'Track Album'),
                    tag('.artist', 'Track Artist')
                ]),
                tag('.additional', [
                    tag('.fa.fa-volume-up'),
                    //tag('fa.fa-rss.active'),
                    tag(`.fa.fa-list-ol(data-uri="${__rootdir}/queue", data-action="toggleUri")`),
                    tag(`.fa.fa-chevron-up(data-uri="${__rootdir}/player", data-action="toggleUri")`),
                    tag(`.fa.fa-expand(data-uri="${__rootdir}/player", data-action="toggleFullscreen")`),
                    tag('.status', [
                        '<span>0:00</span>',
                        '<span>/</span>',
                        '<span>0:00</span>'
                    ])
                ])
            ])
        ]);
    };

    this.build = function(opts) {
        opts = Object.assign({}, opts || {});
        var content = this.compile();
        if (opts.text) {
            return Promise.resolve(content);
        } else {
            $('body > .app > .mini.player').empty().remove();
            $('body > .app').append(content);
            if (document.fullscreen) {
                var button = $('.player .fa.fa-expand');
                if (button.length > 0) {
                    button.removeClass('fa-expand').addClass('fa-compress');
                }
            }
            //return $('body > .app > .mini.player');
            return Promise.resolve();
        }
    };
})());