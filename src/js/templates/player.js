/**
* MiniPlayer Template
*/
(new (function MiniPlayer() {

    this.compile = function() {
        var tag = template.tag;

        var context = controller.getContext();

        if (!(context && context.data && context.queue)) {
            return tag('.fullscreen.engraved.center.error > div', [
                tag('h1', 'Player'),
                tag('h3', 'Start playback to use the fullscreen player')
            ]);
        }

        return tag('.center.player > .container', [
            tag('.track > .information', [
                tag('.cover > .container', [
                    tag('img(src="", width=64, height=64)'),
                    tag('.love.active > .fa.fa-heart')
                ]),
                tag('div', [
                    tag('.name', 'Track Name'),
                    tag('.album', 'Track Album'),
                    tag('.artist', 'Track Artist')
                ]),
                tag('.progress > .tracker > .active'),
                tag('.additional', [
                    tag('.status', [
                        '<span>0:00</span>',
                        '<span>/</span>',
                        '<span>0:00</span>'
                    ])
                ]),
            ]),
            tag('.playback', [
                tag('.fa.fa-backward'),
                tag('.fa.fa-play'),
                tag('.fa.fa-forward'),
            ]),
            tag('.additional.playback', [
                tag('.group-l', [
                    tag('.fa.fa-random'),
                    tag('.fa.fa-repeat'),
                ]),
                tag('group-r', [
                    tag(`.fa.fa-volume-up`),
                    tag(`.fa.fa-list-ol(data-uri="${__rootdir}/queue", data-action="toggleUri")`),
                    tag(`.fa.fa-chevron-down(data-uri="${__rootdir}/player", data-action="toggleUri")`),
                    tag(`.fa.fa-expand(data-uri="${__rootdir}/player", data-action="toggleFullscreen")`),
                ])
            ]),
        ]);
    };

    this.build = function(opts) {
        opts = Object.assign({}, opts || {});

        var content = this.compile();

        $('body').removeClass('showing-mini-player');
        $('body').addClass('overriding-mini-player');

        if (opts.text) {
            return Promise.resolve(content);
        } else {

            template.body.setTitle('Player');

            return template.body.build({view: content, infinite_scroll: false}).then(function() {
                controller.init();
                if (document.fullscreen) {
                    var button = $('.player .fa.fa-expand');
                    if (button.length > 0) {
                        button.removeClass('fa-expand').addClass('fa-compress');
                    }
                }
                template.body.on('onViewRemoved', function() {
                    if (controller.getContext().data) {
                        $('body').addClass('showing-mini-player');
                    }
                    $('body').removeClass('overriding-mini-player');
                });
            });
        }
    };
})());