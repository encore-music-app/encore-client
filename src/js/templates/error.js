/**
 * Error Template
 */
(new (function() {
    this.build = function(opts) {
        var obj = {};
        // Build in view if it's requested but if one is present then
        // we must follow suite!
        var key = (opts.view || template.body.isViewPresent() ? 'view' : (opts.app ? 'app' : 'body'));
        obj[key] = `<div class='center engraved error'><h1 class='title'>${opts.title}</h1><h3 class='title'>${opts.message}</h3>`;
        return template.body.build(obj);
    };
})());