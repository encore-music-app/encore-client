/**
* Playlists Template
*/
(new function Playlists() {
    this.compile = function(data) {
        var tag = template.tag;
        var tagRaw = template.tagRaw;

        if (!((data.playlist && data.tracks) || data.playlists)) {
            return false;
        }

        if (data.playlist && data.tracks) {
            // TODO: show tracks
            var tracks = data.tracks;

            var list = data.tracks.map(function(track) {
                var artists = track.artists.map(function(artist) {
                    return tag('a', {'data-uri': `${__rootdir}/artists/${artist.uri}`}, artist.name)
                }).join(',&nbsp;');

                return tagRaw('tr', null,
                tagRaw('td', null, tag('a', {'data-uri': `${__rootdir}/tracks/${track.uri}`, 'data-action': 'play'}, track.name)) +
                tagRaw('td', null, tag('a', {'data-uri': `${__rootdir}/albums/${track.album.uri}`}, track.album.name)) +
                tagRaw('td', null, artists) +
                '<td><span>' + track.length + '</span></td>'
                );
            });

            var result = '';
            if (!data.opts.append) {
                result = '<caption><h1 class="title">' + data.playlist.name + '</h1></caption>' +
                '<thead><tr>' +
                '<th>Track Name</th>' +
                '<th>Album</th>' +
                '<th>Artists</th>' +
                '<th>Time</th>' +
                '</tr></thead>' +
                '<tbody>' + list.join('') + '</tbody>';
                result = tag('table', {class: 'list' + (data.opts.class ? ' ' + data.opts.class : '')}, result);
            } else {
                result = list.join('');
            }

            if (data.opts.view_more && status.page_count > status.page) {
                result += '<br><div style="text-align: center">' +
                tag('a', {class: 'button', 'data-uri': data.opts.view_more}, 'View More');
            }

            return result;
        }

        var list = data.playlists.map(function(playlist) {
            var uri = `${__rootdir}/playlists/${playlist.uri}`;
            return tagRaw('tr', null,
                // No icons are presented for playlists (yet?)
                // tag('td > img.icon', {src: uri + '/cover'}),
                tag('td > h3', tag('a', {'data-uri': uri}, playlist.name)) +
                tag('td > div.actions', [
                    // Play
                    tag('a.fa.fa-play', {'data-uri': uri, 'data-action': 'play'}),
                    // Rename
                    tag('a.fa.fa-pencil', {'data-uri': uri, 'data-action': 'edit'}),
                    // Delete
                    tag('a.fa.fa-trash', {'data-uri': uri, 'data-action': 'remove'})
                ])
            );
        });

        var result = '';
        if (!data.opts.append) {
            result = '<caption><h1 class="title">' + data.opts.title + '</h1></caption>' +
            '<thead><tr>' +
            //'<th>Icon</th>' +
            '<th>Name</th>' +
            '<th>Actions</th>' +
            '</tr></thead>' +
            '<tbody>' + list.join('') + '</tbody>';
            result = tag('table', {class: 'list' + (data.opts.class ? ' ' + data.opts.class : '')}, result);
        } else {
            result = list.join('');
        }

        if (data.opts.view_more && status.page_count > status.page) {
            result += '<br><div style="text-align: center">' +
            tag('a', {class: 'button', 'data-uri': data.opts.view_more}, 'View More');
        }

        return result;
    };

    this.detectActive = function() {
        // Detect active track in a playlist
        var track = controller.getActiveContext();
        if (!track) {
            return;
        }
        $(".view table tbody tr").removeClass('active');
        $(".view table tbody tr a[data-uri*='tracks/']").each(function(i, e) {
            var match = e.dataset.uri.match(/tracks\/([\w\d]+)/);
            if (track.uri == match[1]) {
                $(e).parents('tr').last().addClass('active');
                return false;
            }
        });
    };

    const getTitle = function(opts) {
        // TODO: Create different titles for different scenarios
        // (e.g. 'Crazy Pops' by <username>, 'Your Playlists', etc)
        // Be able to utilise current data!
        if (!opts.title && opts.params && opts.params.query) {
            if (opts.params.user && opts.params.user !== user) {
                return Promise.resolve(opts.compile);
            }
            if (!opts.params.id) {
                return Promise.resolve('Your Playlists');
            } else {
                return Promise.resolve('Untitled Playlist');
            }
        } else {
            return Promise.resolve(opts.title);
        }
    };

    this.build = function(context, opts) {
        opts = Object.assign({}, opts || {});
        opts.params = opts.params || {};

        var data = (context && context.isOK() ? context.getData() : []);
        opts.data = data;

        template.current.collection = context;
        template.current.template = this;

        if (opts.params.id && data) {
            // data.cover = models.playlists.getCoverUrl({id: data.uri, size: 'medium'});
            models.playlists.getTracks({playlist: opts.params.id}).then((collection) => {
                var tracks = collection.getData();

                template.current.collection = context;

                if (tracks && !collection.isFinished()) {
                    var content = this.compile({
                        playlist: data,
                        tracks: tracks,
                        opts: opts
                    });
                    if (opts.text) return Promise.resolve(content);
                    template.body.setTitle(data.name);

                    if (opts.append) {
                        $('.view > .list.tracks').eq(0).append(content);
                        content = null;
                        this.detectActive();
                    }

                    if (opts.text || opts.append) {
                        return Promise.resolve(content);
                    }

                    delete data;
                    delete tracks;
                    context.clearData();
                    collection.clearData();

                    return template.body.build({view: content});
                } else {
                    return template.error.build({
                        view: true,
                        title: data.name,
                        message: 'This playlist feels lonely without any tracks...'
                    });
                }
            });
        } else if (opts.params.id && !data) {
            return template.error.build({
                view: true,
                title: 'Something has gone wrong :/',
                message: 'Playlist doesn\'t exist'
            });
        } else if (!opts.params.id && data.length > 0) {

            opts.title = opts.title || 'Playlists';
            opts.class = opts.class || 'playlists';

            var promise;
            if (!opts.title) {
                promise = getTitle(opts);
            } else {
                promise = Promise.resolve(opts.title);
            }

            return promise.then((title) => {
                opts.title = title;
                var content = this.compile({
                    opts: opts,
                    playlists: data,
                    context: context
                });

                delete data;
                context.clearData();

                template.body.setTitle(opts.title);
                debugger;
                return template.body.build({view: content}).then(() => {
                    template.tracks.detectActive();if (opts.append) {
                        $('.view > .list.playlists').eq(0).append(content);
                        content = null;
                        this.detectActive();
                    }

                    if (opts.text || opts.append) {
                        return Promise.resolve(content);
                    }
                });
            });
        } else if (!opts.params.id && data.length == 0) {
            if (!(opts.append || opts.text)) {
                return template.error.build({
                    view: true,
                    title: opts.title,
                    message: 'No playlists are available'
                });
            } else {
                content = `<h1>${opts.title}</h1><div class=".list"><h2>No results</h2></div>`;
                return (opts.text || opts.append ? Promise.resolve(content) : template.body.build({view: content}));
            }
        }

    };
}());