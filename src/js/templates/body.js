/**
* Body Template
*/
(new function Body() {
    var title = '';
    var events = {};
    var self = this;

    var fire = function(event) {
        var args = Array.prototype.slice.apply(arguments, [1]);
        if (events.hasOwnProperty(event)) {
            var list = events[event];
            for (var index in list) {
                list[index].apply(list[index], args);
            };
        }
    };

    this.getEvents = function() {
        return events;
    }

    this.on = function(event, callback) {
        if (events.hasOwnProperty(event)) {
            events[event].push(callback);
        } else {
            events[event] = [callback];
        }
        return this;
    };

    this.off = function(event, callback) {
        if (event && events.hasOwnProperty(event)) {
            if (callback && (index = events[event].indexOf(callback))) {
                delete events[event][index];
            } else {
                delete events[event];
            }
        } else if (!event) {
            events = {};
        }
        return this;
    };

    // TODO: decouple this functionality so that it's not dependent on all
    // templates!
    this.on('onDataLeftClick', function(element) {
        if ($(element).parents('.context-menu').length > 0) {
            template.menu.contextual.close();
        }

        console.log(element);

        if (element.dataset.action && element.dataset.uri) {
            // Generic actions allowed so far
            switch (element.dataset.action) {
                case 'play':
                    return controller.setCollection(template.current.collection).then(() => {
                        if (element.dataset.uri && element.dataset.uri.match(/tracks\/([\w\d]+)$/)) {
                            controller.setActiveFromUri(element.dataset.uri);
                        } else {
                            controller.setActive(0);
                        }
                    }).catch(showError);
                case 'play-queue-item':
                    var context = controller.getContext();

                    var track = context.data.filter((e) => e.uri == element.dataset.uri)[0]

                    // The db id of the actual track does not match the queue.
                    // Instead we must match (indexOf) the element in the
                    // array manually. Then we must set the active queue ID by
                    // matching the indexed element in the queue.
                    if (track &&
                        (index = context.data.indexOf(track)) && index > -1 &&
                        (index = context.queue.indexOf(index)) && index > -1) {
                        controller.setActiveFromQueueId(index);

                    } else {
                        // TODO: Show dialog of error!
                    }
                    break;
                case 'loadUri':
                    return loadUri(element.dataset.uri);
                case 'toggleUri':
                    return toggleUri(element.dataset.uri);
                case 'toggleFullscreen':
                    return toggleFullscreen();
                case 'clipboard':
                    // TODO: Finish dialog box
                    /*template.showDialog({
                        title: 'Share',
                        message: '',
                        content: '<p>This is a test dialog',

                        buttons: [
                            {uri: '', action: 'close'}
                        ]
                    })*/;
                    return template.copyToClipboard(router.createLink(element.dataset.uri));
                default:
                    console.error('unknown action', element.dataset);
            }
        }
    });

    this.on('onDataRightClick', function(element, position) {
        if (element.closest('.view') !== null) {
            if (element.dataset.uri.match(/tracks\/([\w\d]+)$/)) {
                template.menu.contextual.build({
                    items: [
                        {
                            label: 'Play',
                            link: element.dataset.uri,
                            action: 'play'
                        },
                        {
                            label: 'Favourite',
                            link: element.dataset.uri,
                            class: 'disabled',
                            action: 'add_favourite'
                        },
                        {
                            label: 'View',
                            link: element.dataset.uri,
                            action: 'loadUri'
                        },
                        {
                            label: 'Go to Artist',
                            link: element.dataset.uri,
                            action: 'loadUri'
                        },
                        {
                            label: 'Go to Album',
                            link: element.dataset.uri,
                            action: 'loadUri'
                        },
                        {
                            label: 'Add to Queue',
                            link: element.dataset.uri,
                            action: 'queue'
                        },
                        {
                            label: 'Add to Playlist',
                            link: element.dataset.uri,
                            action: 'add_to_playlist'
                        },
                        {
                            label: 'Copy Link',
                            link: element.dataset.uri,
                            action: 'clipboard',
                        },
                        {
                            label: 'Properties',
                            link: element.dataset.uri,
                            action: 'toggleUri'
                        },
                        {
                            label: 'Delete',
                            class: 'dangerous',
                            link: element.dataset.uri,
                            action: 'delete'
                        }
                    ],
                    css: position
                })
            } else if (element.dataset.uri.match(/(albums|artists)/)) {

            }
        }
    });

    const setContent = function(content) {
        var insertionTypes = {
            view: {
                selector: 'body > .app > .view',
                event: function() {
                    fire('onViewRemoved');
                    self.off('onViewRemoved');
                    $('.view').off();
                    $('.view')[0].scroll(0,0);
                }
            },
            app: {
                selector: 'body > .app'
            },
            body: {
                selector: 'body'
            }
        };

        var found = false;
        for (var i in insertionTypes) {
            if (content[i]) {
                var t = insertionTypes[i];
                found = true;
                var el = $(t.selector);
                if (t.event) {
                    /*setTimeout(*/t.event();//, 1);
                }
                if (content.append === true) {
                    el.append(content[i]);
                } else {
                    el.empty().html(content[i]);
                }
                break;
            }
        }

        if (found) {
            return content[i];
        } else {
            throw new Error('Invalid content type');
        }
    };

    this.tidyScripts = function() {
        $('script').remove();
    };

    this.isAppPresent = function() {
        return $('.app').length > 0;
    };

    this.isViewPresent = function() {
        return this.isAppPresent() && $('.app > div.view').length > 0;
    };

    const left_click = function(e) {
        var el = e.target;
        if ($(el).is('input') && ['button', 'submit'].indexOf($('el').type) < 0) {
            return;
        }
        e.stopPropagation();
        do {
            var d = el.dataset;
            if (d && Object.keys(d).length > 0) {
                if (d.action) {
                    fire('onDataLeftClick', el);
                } else if (d.uri) {
                    template.current.template = null;
                    template.current.collection = null;
                    loadUri(d.uri);
                } else {
                    fire('onDataLeftClick', el);
                }
                break;
            }
        } while ((el = el.parentNode) && el.tagName !== 'BODY')
        return false;
    };

    const right_click = function(e) {
        var el = e.target;
        e.stopPropagation();
        do {
            if (Object.keys(el.dataset).length > 0) {
                fire('onDataRightClick', el, {left: e.pageX, top: e.pageY + 15});
                break;
            }
        } while ((el = el.parentNode) && el.tagName !== 'BODY');
        return false;
    };

    const form_submission = function(e) {
        e.preventDefault();
        e.stopPropagation();
        var method = e.target.method.toLowerCase();
        if (method === 'get') {
            var uri = __rootdir + e.target.dataset.uri;
            var fields = $(e.target).find('input');
            for (var i in fields) {
                var field = fields[i];
                var placeholder = `:${field.name}`;
                if ((index = uri.indexOf(placeholder)) && index > -1) {
                    uri = uri.replace(placeholder, field.value);
                }
            }
            loadUri(uri);
        } else if (method === 'post') {
            // TODO?
            console.log('unsupported/unimplemented method');
        } else {
            console.log('invalid method: ', method);
        }
        return false;
    };

    this.allowKeyShortcuts = true;

    this.detectScroll = function() {
        if (!template.current.infinite_scroll) {
            return;
        }
        let triggered = false;
        var scrollEvent = function() {
            var view = $(this)[0];
            var viewTotal = view.scrollHeight - view.offsetHeight;
            var viewCurrent = view.scrollTop;
            //var status = template.current.collection.getStatus();
            if (!template.current.infinite_scroll) {
                return;
            }
            if (viewCurrent / viewTotal > 0.75 && !triggered) {
                triggered = true;
                $(view).off('scroll', scrollEvent);
                if (template.current &&
                    template.current.collection &&
                    !template.current.collection.isFinished()) { /* status.page < status.page_count*/
                    template.grabNextInContext(template.current.collection, template.current.template)
                    .then(function() {
                        self.redetect();
                        self.detectScroll();
                    });
                } else {
                    $(view).off('scroll');
                }
            }
        };
        setTimeout(function() {
            $('.view').off('scroll').on('scroll', scrollEvent);
        }, 250);
    };

    this.redetect = function() {
        $('.view').off();

        document.onclick = function(e) {
            left_click(e);
            template.menu.contextual.close();
        };
        document.oncontextmenu = right_click;
        $('form').off().on('submit', form_submission);
        $('form input, form textarea').off()
        .on('focus', function() {
            self.allowKeyShortcuts = false;
        })
        .on('blur', function() {
            self.allowKeyShortcuts = true;
        })

        $('body').off()

        $(window).off();

        $(window).on('resize', function() {
            template.menu.refresh();
        });

        $(window).on('keypress', function(e) {
            // Only allow controller shortcuts when enabled and the controller has
            // an active context!
            if (!this.allowKeyShortcuts || !controller.getActiveContext()) {
                return;
            }
            // console.log('Detected Keypress', e);
            switch (e.keyCode) {
                case 32: // Spacebar (Playback)
                controller.togglePlayback();
                e.preventDefault();
                return false;
                case 77: // M (Mute)
                case 109:
                controller.toggleMute();
                e.preventDefault();
                return false;
                case 82: // R (Repeat)
                case 114:
                controller.toggleRepeat();
                e.preventDefault();
                return false;
                case 83: // S (Shuffle)
                case 115:
                controller.toggleShuffle();
                e.preventDefault();
                return false;
                case 62: // > (Next, Shift + Right Arrow)
                controller.next();
                break;
                case 60: // < (Previous, Shift + Left Arrow)
                controller.previous();
                break;
            }
        });

        this.detectScroll();
        //return Promise.resolve();
    };

    const _setDocumentTitle = function(t) {
        document.title = t ? t + ' - Encore' : 'Encore';
    };

    this.setPlayingTitle = function(t) {
        this.setPlayingTitle.t = t;
        if (t) {
            _setDocumentTitle(t);
        } else {
            _setDocumentTitle(title);
        }
    };

    this.setTitle = function(t) {
        title = t;
        if (!this.setPlayingTitle.t) {
            _setDocumentTitle(title);
        }
    };

    this.getTitle = function() {
        return title;
    };

    this.build = function(content) {
        content = Object.assign({}, content || {view: "<div class='empty'></div>"});
        template.current.infinite_scroll = (content.infinite_scroll === false ? false : true);

        var method = (typeof(content) === 'object' && content.view ? 'isViewPresent' : 'isAppPresent');

        var isPresent = this[method]();
        if (isPresent) {
            setContent(content);
            template.menu.refresh();
            this.redetect();
            return Promise.resolve();
        } else {
            // To remove this you must set this to false after the build!
            return Promise.all([
                template.menu.build({text: true}),
                template.miniplayer.build({text: true})
            ]).then((args) => {
                var [menu, miniplayer] = args;
                delete args;
                var elements = $(`${menu}<div class="view"></div>${miniplayer}`);
                if ($('.app').length == 0) {
                    $('body').append($('<div class="app"></div>'));
                }
                $('body > .app').append(elements);
                controller.init();
                delete menu;
                delete miniplayer;
                delete promises;

                setContent(content);
                this.redetect();
                return Promise.resolve();
            });
        }
    };
});