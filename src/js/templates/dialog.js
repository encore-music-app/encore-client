/**
* Dialog Template
*/
(new function Dialog() {
  this.compile = function(data) {
    return tag('div', {class: 'dialog'}, [
        tag('div', {class: 'shortcuts'}, (function() {
          return data.shortcuts.map(function(shortcut) {
            var content = '';//shortcut.content;
            if (shortcut.icon) {
              content += tag('i', {class: 'fa fa-' + shortcut.icon});
            }
            if (shortcut.text) {
              content += tag('span', null, shortcut.text);
            } else if (shortcut.content) {
              content += shortcut.content;
            }
            return tag('a', {
              href: 'javascript:void();',
              'data-section': 'shortcut',
              'data-action': shortcut.action
            }, content);
          });
        }())),
        tag('div', {class: 'inner'}, [
            tag('h1', {align: 'center', style: 'font-family: Raleway; font-weight: 100; font-size: 3em;'}, data.title),
            tag('p', {align: 'centre'}, tag('span', null, data.message)),
            (function() {
                if (data.error && data.error.message) {
                    return tag('div', {style: 'color: red; text-align: center'}, 'Error: ' + error.message.toLowerCase());
                }
            }()),
            data.content,
            tag('div', {class: 'action'}, (function() {
              return data.actions.map(function(button) {
                return tag('a', {
                  class: 'button',
                  'data-uri': button.uri,
                  'data-action': button.action
                }, button.text);
              });
            }()))
          ])
      ])
  };

  this.show = function() {
    var content = this.compile(opts);
    $('body').prepend(content);
    return Promise.resolve(); // TODO: continue work on this...?
    // return self.quickRender('control/dialog', opts).then(function(dialog) {
    //   $('body').prepend(dialog);
    //   // TODO: Make buttons/shortcuts usable.
    // });
  };
}());