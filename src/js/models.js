/**
* Models.
* This is a client-side JS model handling system.
*/
(new function() {

  return require(`${__dirname}/libs/model.js`).then(function (Model) {

    var Models = (new (function Models() {

      const acceptable_query_keys = [
        'limit',
        'page',
        'id',
        'uri',
        'search'
      ];

      const acceptable_media_query_keys = Array.prototype.slice.call(acceptable_query_keys).concat([
        'track',
        'artist',
        'album',
        'playlist',
        'favourite',
        'belongs_to'
      ]);

      const filterQuery = function(query, filter) {
        filter = filter || acceptable_query_keys;
        if (query && typeof(query) === 'object') {
          for (var i in query) {
            if (filter.indexOf(i) < 0) {
              delete query[i];
            }
          }
          return query;
        } else {
          return undefined;
        }
      };

      const filterMediaQuery = function(query, filter) {
        filter = filter || acceptable_media_query_keys;
        return filterQuery(query, filter);
      };

      this.tracks = new Model('tracks', {
        get: function(opts) {
          opts = Object.assign({}, opts || {});
          opts.query = filterMediaQuery(opts.query);
          //opts.resource = 'tracks';
          return this.parent.get(opts);
        },
        getCoverUrl: function(opts) {
          opts = Object.assign({}, opts || {});
          opts.resource = 'tracks';
          opts.size = opts.size || 'small';
          if (!opts.id) {
            throw new Error('A track ID is required to get a cover URL');
          }
          return this.getPath(`${opts.resource}/${opts.id}/cover/${opts.size}`);
        },
        trackExists: function(opts) {
          opts = Object.assign({}, opts || {});
          opts.query = filterMediaQuery(opts.query);
          opts.unpacker = (data) => data;
          opts.method = 'OPTIONS';
          opts.no_collection = true;
          if (!opts.id) {
            throw new Error('A track ID is required to get a cover URL');
          }
          opts.resource = `tracks/${opts.id}/stream`;
          opts.id = '';
          return this.parent.get(opts);
        }
      });

      this.albums = new Model('albums', {
        get: function(opts) {
          opts = Object.assign({}, opts || {});
          opts.query = filterMediaQuery(opts.query);
          opts.resource = 'albums';
          return this.parent.get(opts);
        },
        getCoverUrl: function(opts) {
          opts = Object.assign({}, opts || {});
          opts.resource = 'albums';
          opts.size = opts.size || 'small';
          if (!opts.id) {
            throw new Error('A album ID is required to get a cover URL');
          }
          return this.getPath(`${opts.resource}/${opts.id}/cover/${opts.size}`);
        },
        getTracks: function(opts) {
          opts = Object.assign({}, opts || {});
          opts.query = filterMediaQuery(opts.query);
          if (!opts.album) {
            return Promise.reject(new Error('An album identifier must be specified!'));
          }
          opts.resource = `albums/${opts.album}/tracks`;
          return this.parent.get(opts);
        }
      });

      this.artists = new Model('artists', {
        get: function(opts) {
          opts = Object.assign({}, opts || {});
          opts.query = filterMediaQuery(opts.query);
          opts.resource = 'artists';
          return this.parent.get(opts);
        },
        getTracks: function(opts) {
          opts = Object.assign({}, opts || {});
          opts.query = filterMediaQuery(opts.query);
          if (!opts.artist) {
            return Promise.reject(new Error('An artist identifier must be specified!'));
          }
          opts.resource = `artists/${opts.artist}/tracks`;
          return this.parent.get(opts);
        },
        getAlbums: function(opts) {
          opts = Object.assign({}, opts || {});
          opts.query = filterMediaQuery(opts.query);
          if (!opts.artist) {
            return Promise.reject(new Error('An artist identifier must be specified!'));
          }
          opts.resource = `artists/${opts.artist}/albums`;
          return this.parent.get(opts);
        }
      });

      this.favourites = new Model('favourites', {
        get: function(opts) {
          opts = Object.assign({}, opts || {});
          opts.query = filterMediaQuery(opts.query);
          opts.resource = 'users/current/favourites';
          return this.parent.get(opts);
        },
      });

      this.playlists = new Model('playlists', {
        get: function(opts) {
          opts = Object.assign({}, opts || {});
          opts.query = filterMediaQuery(opts.query);
          opts.resource = 'playlists';
          return this.parent.get(opts);
        },
        getTracks: function(opts) {
          opts = Object.assign({}, opts || {});
          opts.query = filterMediaQuery(opts.query);
          if (!opts.playlist) {
            return Promise.reject(new Error('A playlist identifier must be specified!'));
          }
          opts.resource = `playlists/${opts.playlist}/tracks`;
          return this.parent.get(opts);
        },
      });

      this.sessions = new Model('sessions', {
        get: function(opts) {
          opts = Object.assign({}, opts || {});
          opts.query = filterQuery(opts.query);
          opts.resource = 'sessions';
          return this.parent.get(opts);
        },
      });

      this.users = new Model('users', {
        get: function(opts) {
          opts = Object.assign({}, opts || {});
          opts.query = filterQuery(opts.query);
          opts.resource = opts.resource || 'users';
          return this.parent.get(opts);
        },
        deleteSession: function(session) {
          return this.delete({resource: 'sessions', id: session || this.session});
        },
        deleteUser: function(username) {
          return this.delete({resource: 'users', id: username});
        },
        updateUser: function(username, data) {
          return this.update({resource: 'users', id: username, data: data});
        },
        createUser: function(data) {
          return this.create({resource: 'users', data: data});
        },
        isLoggedIn: function() {
          return this.get({resource: '/', no_collection: true})
          .then((api) => {
            if (api && api.data && api.data.user && api.data.session) {
              this.user = api.data.user;
              this.session = api.data.session;
              return Promise.resolve(this);
            } else {
              return Promise.reject(new Error('Unauthorized'));
            }
          });
        },
        authorise: function(username, password) {
          if (!(username.match(/[\w\d_]+/) && password.length >= 8)) {
            return Promise.reject(new Error('Invalid Credentials'));
          }
          return this.get({auth: {username: username, password: password}, no_collection: true})
          .then((api) => this.isLoggedIn())
          .then(() => {
            window.location.pathname = `${__rootdir}/`;
          }).catch((err) => {
            console.error(err);
            if (err.status === 401 || err.status.message.toLowerCase() == 'unauthorized') {
              return Promise.reject(new Error('Invalid Credentials'));
            } else {
              return Promise.reject(new Error('An error occurred during login'));
            }
          });
        }
      });

      this.search = new Model('search', {
        get: function(opts) {
          opts = opts || {};
          var self = this;
          return this.parent.get({id: opts.query || ''}).then(function(context) {
            if (context && context.isOK() && (searched = context.getData())) {
              var status = context.getStatus();
              var data = context.getData();
              var collection = ['artists', 'albums', 'tracks'];
              collection.forEach(function(i) {
                var temp_data = data[i].data;
                delete data[i].data;
                data[i] = {status: Object.assign(data[i], status), data: temp_data};
                data[i] = new Model.prototype.Collection(data[i], {query: {search: opts.query}, referer: models[i]});
              });

              context = new Model.prototype.Collection({status: status, data: data});
              context.isOK = function() {
                var data = context.getData();
                return (
                  data.tracks.isOK() &&
                  data.artists.isOK() &&
                  data.albums.isOK()
                );
              };
              return context;
            } else {
              // Let's ignore at this point
              return context;
            }
          })
        }
      });

      this.on = Model.on;
      this.off = Model.off;

      return this;
    })());

    Models.prototype = Model;

    return Models;

  });

});
