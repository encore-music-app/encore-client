(function() {

  __jspath = document.currentScript.src.split('/');
  __jspath.pop();
  __jspath = __jspath.join('/');

  loadUri = function(url) {
    history.pushState(null, null, url);
    router.setPath(window.location.pathname.replace(__rootdir, ''));
    $('.view').off();
    router.run();
    template.menu.refresh();
  };

  toggleUri = function(url) {
    if (window.location.pathname == url) {
      history.back(-1);
    } else {
      loadUri(url);
    }
  };

  toggleFullscreen = function() {
    if (document.fullscreen) {
      document.exitFullscreen();
      var btn = $('.fa.fa-compress[data-action=\'toggleFullscreen\']');
      if (btn.length > 0) {
        btn.removeClass('fa-compress').addClass('fa-expand');
      }
    } else {
      document.body.requestFullscreen().then(function() {
        var btn = $('.fa.fa-expand[data-action=\'toggleFullscreen\']');
        if (btn.length > 0) {
          btn.removeClass('fa-expand').addClass('fa-compress');
        }
      });
    }
  }

  var instance_id = (function (len, chars) {
    len = len || 10;
    chars = chars || "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var text = "";
    for (var i = 0; i < len; i += 1) {
        text += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return text;
  }());

  var original = {log: console.log, error: console.error, warn: console.warn};
  console.log = function() {
    var args = Array.prototype.slice.apply(arguments);
    original.log.apply(console, args);
    args = args.map((e) => (typeof(e) == 'object' ? JSON.stringify(e) : e.toString())).join(' ');
    $.ajax({
      url: __rootdir + '/api/v1/report',
      method: 'post',
      dataType: 'json',
      data: {instance: instance_id, log: args}
    });
  };

  console.error = function() {
    var args = Array.prototype.slice.apply(arguments);
    original.error.apply(console, args);
    args = args.map((e) => (typeof(e) == 'object' ? JSON.stringify(e) : e.toString())).join(' ');
    $.ajax({
      url: __rootdir + '/api/v1/report',
      method: 'post',
      dataType: 'json',
      data: {instance: instance_id, error: args}
    });
  };

  console.warn = function() {
    var args = Array.prototype.slice.apply(arguments);
    original.error.apply(console, args);
    args = args.map((e) => (typeof(e) == 'object' ? JSON.stringify(e) : e.toString())).join(' ');
    $.ajax({
      url: __rootdir + '/api/v1/report',
      method: 'post',
      dataType: 'json',
      data: {instance: instance_id, warn: args}
    });
  };

  showError = function(state) {
    if (state && state.message && state.message.toLowerCase() === 'unauthorized') {
      loginRedirect();
      return;
    } else if (state && state.getStatus && typeof(state.getStatus) === 'function') {
      state = state.getStatus();
      if (state.reason) {
        state.message = [state.message, '(' + state.code + ')', '-', state.reason].join(' ');
      } else {
        state.message = state.code + ' - ' + state.message;
      }
    }
    var message = 'Sorry for the inconvenience';
    if (state.status && state.status.message) {
      message = state.status.message;
    } else if (state.message) {
      message = state.message;
    }
    debugger;
    console.error(state);
    return template.error.build({
      view: true,
      title: 'An Error Occurred!',
      message: message
    });
  };

  const loginRedirect = function() {
    window.location.pathname = __rootdir + '/login';
  };

  require.setBasePath(__jspath);
  return require([
    '/libs/controller.js',
    '/libs/router.js',
    '/models.js',
    '/template.js',
  ], function(Controller, Router, models, template) {
    // Replace any prefixed non-root directory (i.e. proxied folders, etc)
    var rootdir = Router.prototype.absolute(window.location.pathname, __rootdir);
    var relative_path = window.location.pathname.replace(rootdir, '');
    __rootdir = rootdir;

    var router = new Router(relative_path);
    var controller = new Controller();

    window.models = models;
    window.template = template;
    window.router = router;
    window.controller = controller;

    controller.off();

    var changing_status = false;
    //var force_playback = false;

    var updateStatus = function() {
      if (changing_status) {
        return;
      }
      changing_status = true;
      if (!$('body').is('.showing-mini-player') && !$('body').is('.overriding-mini-player')) {
        $('body').addClass('showing-mini-player');
      }

      var track = controller.getActiveContext();

      if ('mediaSession' in navigator) {
        var metadata = (navigator.mediaSession.metadata || {});
        metadata.title = track.name;
        metadata.artist = track.artists.map(function(a) { return a.name }).join(', ');
        metadata.album = track.album.name;
        metadata.artwork = [
          // { src: 'https://dummyimage.com/96x96',   sizes: '96x96',   type: 'image/png' },
          // { src: 'https://dummyimage.com/128x128', sizes: '128x128', type: 'image/png' },
          // { src: 'https://dummyimage.com/192x192', sizes: '192x192', type: 'image/png' },
          // { src: 'https://dummyimage.com/256x256', sizes: '256x256', type: 'image/png' },
          // { src: 'https://dummyimage.com/384x384', sizes: '384x384', type: 'image/png' },
          { src: router.createLink(models.tracks.getCoverUrl({id: track.uri, size: 'large'})), sizes: '512x512', type: 'image/png' },
        ];

        //if (!navigator.mediaSession.metadata) {
          navigator.mediaSession.metadata = new MediaMetadata(metadata);

          navigator.mediaSession.setActionHandler('play', function() {controller.play()});
          navigator.mediaSession.setActionHandler('pause', function() {controller.pause()});
          // navigator.mediaSession.setActionHandler('seekbackward', function() {});
          // navigator.mediaSession.setActionHandler('seekforward', function() {});
          navigator.mediaSession.setActionHandler('previoustrack', function() {
           // force_playback = true;
            controller.previous();
            return true;
          });
          navigator.mediaSession.setActionHandler('nexttrack', function() {
            force_playback = true;
            controller.next();
            return true;
          });
        //}

        navigator.mediaSession.playbackState = (controller.isPlaying() ? 'playing' : 'paused');
        changing_status = false;
      }

      if (controller.isPlaying()) {
        var track = controller.getActiveContext();
        if (track) {
          var text_artists = track.artists.map(function(a) { return a.name }).join(', ');
          template.body.setPlayingTitle(['▶', track.name, 'by', text_artists].join(' '));
        }
      } else {
        template.body.setPlayingTitle(template.body.getTitle());
      }
    };

    // controller.on('context-load', function() {
    //   // Android media control changes require a forced playback :(
    //   if (force_playback) {
    //     controller.play();
    //     force_playback = false;
    //   }
    // });

    controller.on('playback-error', function(track) {
      // TODO: Show alert box on playback error!
      console.log(`Unable to play track "${track.name}"`);
      force_playback = true;
      controller.next();
    });

    controller.on('playback-change', updateStatus);

    controller.on('context-source-change', function(track) {
      if (!track) {
        return;
      }

      updateStatus();
      template.tracks.detectActive();
    });

    window.onpopstate = function() {
      template.menu.refresh();
      router.setPath(window.location.pathname.replace(__rootdir, ''));
      router.run();
    };

    //var start = Date.now();
    return models.users.isLoggedIn()
    .then(({user, session}) => {
      //console.log('Active user & session:', user, session, Date.now() - start + 'ms');
      return router
      .resolve('/login', function() {
        window.location.pathname = rootdir;
      })
      .resolve('/logout', function() {
        models.users.deleteSession(models.users.session).then(() => {
          window.location.pathname = rootdir;
        }).catch((err) => {
          console.error(err);
          showError(err);
        });
        delete res;
      })
      .resolve('/:kind(tracks|albums|artists|playlists)/:id?', function(res) {
        var kind = res.params.kind;
        var id = res.params.id;
        var params = {id: id, query: res.query, limit: 100};
        return models[kind].get(params).then(function(collection) {
          return template[kind].build(collection, {params: params});
        }).catch(showError);
      })
      .resolve('/favourites', (res) => {
        return models.favourites.get().then((collection) => {
          if (collection.getData().length > 0) {
            return template.tracks.build(data, {title: 'Favourites'});
          } else {
            return template.error.build({view: true, title: 'Favourites', message: 'Add some tracks to make this place feel like home...'});
          }
        }).catch(showError);
      })
      .resolve('/search/:query?', (res) => {
        if (res.params.query == '' || !res.params.query) {
          return template.error.build({view: true, title: 'Whoops!', message: 'The search term is too generic!'});
        } else {
          return models.search.get(res.params)
          .then(function(collection) {
            //if (collection.isOK()) {
              return template.search.build(collection, {params: res.params});
            // } else {
            //   return template.error.build({view: true, title: 'Whoops!', message: 'The search returned no results :/'});
            // }
          })
          .catch(showError);
        }
      })
      .resolve('/player', (res) => template.player.build())
      .resolve('/queue', (res) => template.queue.build())
      .resolve('/settings/:subsection?', function(res) {
        template.settings.build()
      })
      .resolve('/:kind(library)?', function(res) {
        // Root path and 'your library'
        // TODO: Go to 'library' page which will contain a list of recent music
        template.body.build({view: '<h1>Welcome Back, ' + user + '</h1>'});
      })
      .on('after', (match) => {
        //console.log('Routing match:', match);
        if (!match) {
          template.error.build({body: true, title: 'Uh Oh!', message: 'This page doesn\'t exist!'});
        }
      })
      .on('error', showError);

    })
    .catch((err) => {
      console.log('Not logged in or unable to check authorization', err);
      return router.resolve('/login', (res) => {
        return template.login.build();
      }).on('after', (match) => {
        if (!match) {
          loadUri(__rootdir + '/login');
          loginRedirect();
        }
      })
    });
  })
  .then(function(router) {
    return router.run();
  })
  .catch((e) => {debugger; console.error(e.stack)});
  // var start = require(`${__jspath}/routing.js`)
  // .then(function(router) {
  // 	return router.run();
  // })
  // .then(function(router) {
  // 	delete router;
  // 	delete start;
  // 	return;
  // });

}());
