/**
* Controller.
* This is a controller script for playback control.
*/
(new function() {
  return require(`${__dirname}/random.js`, function(Random) {
    return (function Controller(selector) {
      const self = this;
      selector = selector || '.player';
      var events = {};
      // The context contains the collection, the active item and the queue?
      var context = {};
      var volume = 0.5;
      var repeat = 'none';
      var shuffle = false;
      var is_playing = false;
      var is_initiated = false;
      var _audio = document.createElement('audio');
      var elements = {};
      var focused = true;
      var isSafari = navigator.userAgent.match(/Version\/[\d.]+ Safari\/[\d.]+$/);

      const fire = function(event) {
        var args = Array.prototype.slice.apply(arguments, [1]);
        console.log('firing', event);
        if (events.hasOwnProperty(event)) {
          var list = events[event];
          for (var index in list) {
            list[index].apply(list[index], args);
          };
        }
      };

      this.on = function(event, callback) {
        if (events.hasOwnProperty(event)) {
          events[event].push(callback);
        } else {
          events[event] = [callback];
        }
        return this;
      };

      this.off = function(event, callback) {
        if (event && events.hasOwnProperty(event)) {
          if (callback && (index = events[event][callback])) {
            delete events[event][index];
          } else {
            delete events[event];
          }
        } else if (!event) {
          events = {};
        }
        return this;
      };

      const timeUpdateHeavy = function() {
        // Check for focus events just in case this function is still being used
        // in the event handler.
        if (_audio.currentTime && _audio.duration && focused) {
          var dt = new Date(Date.now());
          //console.log('updated ' + dt.toUTCString());
          elements.time_processed.text(seconds_to_str(_audio.currentTime));
          elements.time_remaining.text(seconds_to_str(_audio.duration));
          elements.tracker_progress.css({
            //transform: scale(' + (_audio.currentTime / _audio.duration) + ', 1)'
            // Round width percentage to 4dp.
            width: Math.round((_audio.currentTime / _audio.duration) * 1000000) / 10000 + '%'
          });
        }
        if (isSafari) {
          timeUpdateLight();
        }
      };

      const timeUpdateLight = function() {
        if (_audio.currentTime >= _audio.duration) {
            onEnded();
        }
      };

      const onEnded = function() {
        elements.tracker_progress.css({
          //transform: 'scale(0,1)'
          width: '0%'
        });
        if (!_audio.loop) {
          self.next();
        }
      };

      const onFocus = function() {
        focused = true;
        _audio.ontimeupdate = timeUpdateHeavy;
      };

      const onBlur = function() {
        focused = false;
        if (isSafari) {
          _audio.ontimeupdate = timeUpdateLight;
        } else {
          _audio.ontimeupdate = undefined;
        }
      };

      const onWindowFocusIndeterminate = function(e) {
        var h = 'hidden';
        var v = 'visible';

        var kind = {
          focus: 0,
          blur: 1,
          pageshow: 0,
          pagehide: 1,
          visibilitychange: ~~document.hidden
        };

        var isFocused = !kind[e.type];
        //var kindName = ['hidden', 'visible'];
        //var isFocusedName = kindName[~~isFocused];

        if (isFocused) {
          onFocus();
        } else {
          onBlur();
        }
      };

      const onplayable = function() {
        // Assigned to audio element to automatically setup controller and play song
        elements.tracker_progress.css({width: '0%'});

        fire('context-source-change', self.getActiveContext());

        _audio.ontimeupdate = (
          focused ?
          timeUpdateHeavy :
          (isSafari ? timeUpdateLight : undefined)
        );

        _audio.onplaying = function() {
          is_playing = true;
          elements.tracker_progress.removeClass('loading');
          fire('playback-change', self.getPlaybackState());
          update_ui();
        };

        _audio.onpause = function() { self.setPlayState(false) };

        if (!isSafari) {
          // Safari is not reliable telling us that the track ends
          _audio.onended = onEnded;
        }

        _audio.play();
      };

      const onclickcontrols = function(e) {
        // Whenever a user clicks the controller
        var button = e.target;
        var name = button.className;

        if ((!!~name.indexOf('play') || !!~name.indexOf('pause')) && name.indexOf('playback') == -1) {
          if (is_playing) {
            self.pause();
          } else {
            self.play();
          }
        }

        if (!!~name.indexOf('repeat')) {
          self.toggleRepeat();
          return;
        }

        if (!!~button.className.indexOf('random')) {
          self.setShuffle(!shuffle);
          return;
        }

        if (!!~name.indexOf('backward')) {
          self.previous();
          return;
        } else if (!!~name.indexOf('forward')) {
          self.next();
          return;
        }

        if (!!~name.indexOf('volume')) {
          self.toggleMute();
          return;
        }

        //e.stopPropagation();

      };

      const onclicktracker = function(e) {
        // Move song position from the current tracker position
        // TODO: Fix e.clientX issues with element placement!
        if (_audio) {
          var pauseafter = _audio.paused;
          _audio.currentTime = (e.clientX / this.offsetWidth) * _audio.duration;
          if (pauseafter) setTimeout(() => _audio.pause(), 10);
        }
      };

      const onmousedowntracker = function(e) {
        // 'Propose' the position of the tracker to change song position
        this.querySelector('.active').style.width = e.layerX / this.offsetWidth;
      };

      const onhovertrack = function(e) {
        // TODO: Show time in tooltip above tracker
      };

      const seconds_to_str = function(time) {
        var milliseconds = Math.abs(time - Math.floor(time));
        var seconds = Math.floor(time);
        var minutes = Math.floor(time / 60);
        seconds = Math.floor(time % 60);
        var hours = Math.floor(minutes / 60);
        seconds = (seconds > 9 ? seconds : '0' + seconds);
        return (hours ? hours + ':' + (minutes > 9 ? minutes : '0' + minutes) : minutes) + ':' + seconds;
      };

      const update_ui = function() {
        if (!is_initiated) {
          console.warn('Unable to update controller UI without initialisation');
          return;
        }

        if (elements.repeat_button) {
          elements.repeat_button[repeat === 'track' ? 'addClass' : 'removeClass']('once');
          elements.repeat_button[repeat !== 'none' ? 'addClass' : 'removeClass']('active');
        }

        if (elements.shuffle_button) {
          elements.shuffle_button[shuffle ? 'addClass' : 'removeClass']('active');
        }

        elements.play_button.attr('class', 'fa fa-' + (is_playing ? 'pause' : 'play'));

        var track = self.getActiveContext();

        if (track) {
          elements.track.html(`<a data-uri='${__rootdir}/tracks/${track.uri}'>${track.name}</a>`);
          elements.album.html(`<a data-uri='${__rootdir}/albums/${track.album.uri}'>${track.album.name}</a>`);
          elements.artists.html(track.artists.slice(0, 5).map(function(a) {
            return `<a data-uri='${__rootdir}/artists/${a.uri}'>${a.name}</a>`;
          }).join(', '));
          elements.cover.attr('src', models.tracks.getCoverUrl({id: track.uri, size: 'medium'}));
          elements.cover.css('display', 'inherit');
        } else {
          elements.track.html('Unknown Track');
          elements.album.html('Unknown Album');
          elements.artists.html('Unknown Artist');
          elements.cover.css('display', 'none');
          elements.cover.attr('src', models.tracks.getCoverUrl({id: 'unknown', size: 'medium'}));
        }

        fire('ui-update');

      };

      const _playInternal = function(file, loop, type) {
        if (!is_initiated) {
          self.init();
        }

        var hasExistingSrc = true;
        if (!_audio) {
          hasExistingSrc = false;
          _audio = document.createElement('audio');
        }
        _audio.setAttribute('autoplay', true);
        _audio.setAttribute('preload', 'none');
        if (type) {
          _audio.type = type
        }
        _audio.volume = volume;
        _audio.loop = (repeat === 'track' ? true : false);

        //_audio.src = file;
        _audio.removeAttribute('src');

        var compatibleSources = {
          'mp3': 'audio/mpeg',
          'ogg': 'audio/ogg',
          'wav': 'audio/wav'
        }

        // Remove all source nodes
        while(_audio.childNodes.length > 0) {
          _audio.removeChild(_audio.childNodes[0]);
        }

        // Set source nodes
        for (var i in compatibleSources) {
          if (compatibleSources.hasOwnProperty(i)) {
            var source = document.createElement('source');
            console.log(file);
            source.setAttribute('src', `${file}/${i}`);
            source.setAttribute('type', compatibleSources[i]);
            _audio.appendChild(source);
          }
        }

        if (hasExistingSrc) {
          _audio.load();
        }

        _audio.currentTime = 0;
        _audio.play();

        fire('context-load', self.getActiveContext());

        return _audio;
      };

      const _getSeed = function() {
        return Date.now() % (2 ** 64);
      };

      const _shuffle = function(array, seed) {
        var random = new Random(seed || _getSeed());
        // Using the Fisher-Yates shuffle and a pseudo-random
        // generator so that the random queue can be saved if wanted
        var m = array.length, t, i;

        // While there remain elements to shuffle…
        while (m) {

          // Pick a remaining element…
          i = Math.floor(random.nextFloat() * m--);

          // And swap it with the current element.
          t = array[m];
          array[m] = array[i];
          array[i] = t;
        }

        return array;
      };

      const _updateQueue = function() {
        if (context && context.data) {
          var list = new Array(context.data.length);

          for (var i = 0; i < list.length; i++) {
            list[i] = i;
          }

          if (shuffle) {
            context.seed = _getSeed();
            list = _shuffle(list, context.seed);

            // Move currently playing track to the beginning of the shuffle list
            var index = (context.queue ? context.queue.indexOf(context.active) : null);
            if (index !== null && index > -1) {
              delete list[index];
              list = list.filter((e) => e);
              list.unshift(context.active);
            }
          }

          context.queue = list;
        }
      };

      const _playActive = function() {
        if (!context.active.toString().match(/^\d+$/) && context.data && context.queue) {
          context.active = context.queue[0];
        }
        if (context.active > -1) {
          if (_audio) {
            _audio.pause();
            _audio.remove();
          }

          elements.tracker_progress.addClass('loading');
          elements.tracker_progress.css({
            //transform: 'scale(0,1)'
            width: '0%'
          });

          var track = self.getActiveContext();

          update_ui();

          var format = 'mp3';
          var ENDPOINT = __rootdir + '/api/v1/tracks';
          var url = `${ENDPOINT}/${track.uri}/stream`;

          if (track && track.type == 'external') {
            _playExternal(track.uri, (_audio ? _audio.loop : false));
            _audio.oncanplay = onplayable;
            _audio.onerror = function() {
              update_ui();
              fire('playback-error', track);
            };
          } else {
            models.tracks.trackExists({id: track.uri}).then(function() {
              _playInternal(url, (_audio ? _audio.loop : false));
              _audio.oncanplay = onplayable;
              _audio.onerror = function() {
                update_ui();
                fire('playback-error', track);
              };
            }).catch(function(err) {
              console.error('Track does not exist! (model.track.trackExists)', track);
              self.next();
            });
          }
        }
      };

      this.init = function() {

        // Intialises the player controls
        elements = {
          tracker:          $('.tracker', $(selector)),
          tracker_progress: $('.tracker .active', $(selector)),
          time_processed:   $('.status span:first-child', $(selector)),
          time_remaining:   $('.status span:last-child', $(selector)),
          repeat_button:    $('.playback .fa-repeat', $(selector)),
          shuffle_button:   $('.playback .fa-random', $(selector)),
          play_button:      $('.fa.fa-play, .fa.fa-pause', $(selector)),
          track:            $('.track .information .name', $(selector)),
          album:            $('.track .information .album', $(selector)),
          artists:          $('.track .information .artist', $(selector)),
          cover:            $('.track .cover img', $(selector)),
        }

        elements.tracker.off('click').on('click', onclicktracker);

        elements.tracker.off('mousedown').on('mousedown', onmousedowntracker);

        elements.tracker.off('mousehover').on('mousehover', onhovertrack);

        $(selector).off('click').on('click', onclickcontrols);

        // Set focus and unfocus events
        window.removeEventListener('blur', onWindowFocusIndeterminate);
        window.removeEventListener('focus', onWindowFocusIndeterminate);
        window.removeEventListener('pagehide', onWindowFocusIndeterminate);
        window.removeEventListener('pageshow', onWindowFocusIndeterminate);
        document.removeEventListener('visibilitychange', onWindowFocusIndeterminate);

        window.addEventListener('blur', onWindowFocusIndeterminate);
        window.addEventListener('focus', onWindowFocusIndeterminate);
        window.addEventListener('pagehide', onWindowFocusIndeterminate);
        window.addEventListener('pageshow', onWindowFocusIndeterminate);
        document.addEventListener('visibilitychange', onWindowFocusIndeterminate);

        is_initiated = true;

        update_ui();

        return this;
      };

      this.getPlaybackState = function() {
        return (is_playing ? 'playing' : 'paused');
      };

      this.isPlaying = function() {
        return is_playing;
      };

      this.setPlayState = function(state) {
        if (typeof(state) !== 'boolean') {
          return false;
        }
        var _fire = false;
        if (is_playing !== state) {
          _fire = true;
        }
        is_playing = state;
        if (state) {
          if ((isNaN(parseInt(context.active)) || !_audio) && context.data) {
            _playActive();
          } else if (_audio) {
            _audio.play();
          } else {
            _fire = false;
          }
        } else {
          if (_audio) {
            _audio.pause();
          }
        }
        update_ui();
        if (_fire) {
          fire('playback-change', this.getPlaybackState());
          fire(this.getPlaybackState() == 'playing' ? 'play' : 'pause');
        }
      };

      this.setCollection = function(val) {
        // value must only contain data and active id
        if (!context.collection || context.collection.getOpts() !== val.getOpts()) {
          // Duplicate to avoid issues with other functions using the object.
          context.collection = Object.assign({}, val);
        }
        delete context.data;
        // Update the collection
        return context.collection.getAll().then((collection) => {
          context.data = collection.getData();
          collection.clearData();
          delete collection;
          _updateQueue();
          fire('context-update', context);
          return this;
        });
      };

      this.setActiveFromUri = function(uri) {
        var matches = uri.match(/tracks\/([\w\d]+)/);
        if (matches) {
          uri = matches[1];
          var f = context.data.filter((e) => e.uri === uri);
          if (f && f.length > 0) {
            context.active = context.data.indexOf(f[0]);
            _playActive();
            return true;
          }
        }
        return false;
      };

      this.setActive = function(i) {
        if (i > -1 && i < context.data.length && context.data[i]) {
          context.active = i;
          _playActive();
          return true;
        }
        return false;
      };

      this.setActiveFromQueueId = function(id) {
        if (id < context.queue.length && context.queue[id]) {
          context.active = context.queue[id];
          _playActive();
          return true;
        }
        return false;
      };

      this.getCollection = function() {
        return context.collection;
      };

      this.getActive = function() {
        return context.active;
      };

      this.getActiveContext = function() {
        // Get active context (currently playing track)
        if (context.data && context.data.indexOf(context.active)) {
          return context.data[context.active];
        } else {
          //return {title: 'Unknown', album: 'Unknown', artist: 'Unknown'};
        }
      };

      this.getContext = function() {
        return context;
      };

      this.getQueue = function() {
        // Returns context with any affected changes (e.g. shuffle)
        return context.queue;
      };

      this.next = function() {
        var i = context.queue.indexOf(context.active);
        var current = context.active;

        if (i === context.queue.length - 1 && repeat === 'all') {
          context.active = context.queue[0];
        } else if (i === -1) {
          throw new Error('Unknown queue or active track');
        } else if (i < context.queue.length - 1){
          context.active = context.queue[++i];
        }

        if (current !== context.active) {
          _playActive();
        }

        fire('next');
      };

      this.previous = function() {
        var i = context.queue.indexOf(context.active);
        var current = context.active;

        if (i === 0 && repeat === 'all') {
          context.active = context.queue.length - 1;
        } else if (i === -1) {
          throw new Error('Unknown queue or active track');
        } else if (i > 0) {
          context.active = context.queue[--i];
        }

        if (current !== context.active) {
          _playActive();
        }

        fire('previous');
      };

      this.pause = function() {
        this.setPlayState(false);
      };

      this.play = function() {
        this.setPlayState(true);
      };

      this.togglePlayback = function() {
        this.setPlayState(!is_playing);
      };

      this.setRepeat = function(val) {
        var acceptable = ['track', 'all', 'none'];
        if (val !== repeat && acceptable.indexOf(val) > -1) {
          repeat = val;
          if (val === 'track') {
            _audio.loop = true;
          } else {
            _audio.loop = false;
          }
          update_ui();
          fire('repeat', this);
        }
      };

      this.getRepeat = function() {
        return repeat;
      };

      this.toggleRepeat = function() {
        switch (repeat) {
          case 'none':
            return this.setRepeat('track');
          case 'track':
            return this.setRepeat('all');
          case 'all':
            return this.setRepeat('none');
        }
      };

      this.setShuffle = function(val) {
        if (shuffle !== val) {
          shuffle = (val === true);
          _updateQueue();
          fire('shuffle', context);
          fire('context-update', context);
        }
        update_ui();
      };

      this.toggleShuffle = function() {
        this.setShuffle(!shuffle);
      };

      this.getShuffle = function() {
        return shuffle;
      }

      this.setVolume = function(val) {
        volume = (val >= 0.00 && val <= 1.00 ? val : volume);
        _audio.volume = volume;
        update_ui();
        fire('volume-change', volume);
      };

      this.getVolume = function() {
        return (_audio.mute ? 0 : volume);
      };

      this.unmute = function() {
        _audio.muted = false;
        update_ui();
        fire('unmute');
      };

      this.mute = function() {
        _audio.muted = true;
        update_ui();
        fire('mute');
      };

      this.toggleMute = function() {
        _audio.muted = !_audio.muted;
        update_ui();
        fire(_audio.muted ? 'mute' : 'unmute');
      };

      this.getAudio = function() {
        // This function should be deleted!
        return _audio;
      };

    });
  }, true);
}());
