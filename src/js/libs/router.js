/**
 * Router.
 * This is a client-side JS router.
 */
(function() {

  function Router(path) {
    path = (path ? path : window.location.pathname);
    var routes = {};
    var events = {};
    var halted = false;

    var fire = function(event) {
      var args = Array.prototype.slice.apply(arguments, [1]);
      if (events.hasOwnProperty(event)) {
        var list = events[event];
        for (var index in list) {
          list[index].apply(list[index], args);
        };
      }
    };

    var getQuery = function(query) {
      // This function is not multi-dimensional (e.g. ?hello[world]=test)
      query = query || window.location.search;

      if (query == '') {
        return {};
      }

      if (query[0] === '?') {
        query = query.substring(1);
      }

      var pairs = query.split("&");
      var obj = {};

      for (i in pairs) {
        var pair = pairs[i].split("=");
        obj[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
      }

      return obj;
    };

    var matchPattern = function(pattern) {
      var uri = path;
      var keys = [];
      var regex = new RegExp('^' + pattern.replace(/(\/:([^\/]+))/g, function (full, dots, name) {
        var optional = '';
        if (name[name.length - 1] == '?') {
          optional = '?';
          name = name.slice(0, -1);
        }
        var pattern = '(?:\/([^\/]+))';

        // Allow group expectations (e.g. '/:user(bob|alice)')
        if ((match = name.match(/\([\w\_\d\|]+\)/)) && match) {
          name = name.replace(match[0], '');
          // brackets included in match[0]
          pattern = '(?:\/' + match[0] + ')';
        }

        keys.push(name);
        return pattern + optional;
      }) + '(?:\/|$)');
      //console.log(regex, uri);
      var matches = uri.match(regex);
      if (!matches) {
        return undefined;
      }
      var match = matches[0];
      matches = (matches
        .slice(1, matches.length)
        .reduce((params, value, index) => {
          if (params === null) params = {};
          params[keys[index]] = decodeURIComponent(value || '');
          return params;
        }, null)
        );
      return {
        path: uri,
        matched: match,
        params: matches,
        root: window.location.href.replace(match, ''),
        protocol: window.location.protocol + '//',
        query: getQuery()
      };
    };

    this.getPath = function() {
      return path;
    };

    this.setPath = function(str) {
      path = str;
    };

    this.on = function(event, callback) {
      if (events.hasOwnProperty(event)) {
        events[event].push(callback);
      } else {
        events[event] = [callback];
      }
      return this;
    };

    this.off = function(event, callback) {
      if (events.hasOwnProperty(event) && (index = events[event].indexOf(callback)) > -1) {
        delete events[event][index];
      }
      return this;
    };

    this.remoteAllEventListeners = function(event) {
      if (events && event && events.hasOwnProperty(event)) {
        delete events[event];
      } else if (events && event && !events.hasOwnProperty(event)) {
          // Do nothing - prevent all being wiped!
        } else {
          events = {};
        }
      };

    this.halt = function() {
      // This state will only last once per run before getting reset!
      // This allows the 'before' event to prevent routing to occur.
      halted = true;
    };

    this.run = function() {
      fire('before');
      if (!halted) {
        for (var pattern in routes) {
          if (
            routes.hasOwnProperty(pattern) &&
            (matches = matchPattern(pattern))
            ) {
            try {
              routes[pattern](matches);
              fire('after', matches);
            } catch (e) {
              fire('error', e);
            }
            return true;
          }
        }
        fire('after');
        return false;
      } else {
        // The after event will not fire due to the fact a halt prevents
        // any routing to occur and should therefore not fire!
        // We must also prevent the halted event from keeping a halted
        // state before the next possible recursion.
        fire('halted');
        halted = false;
        return false;
      }
    };

    this.resolve = function(uri, callback) {
      if (!routes.hasOwnProperty(uri)) {
        routes[uri] = callback;
        return this;
      } else {
        throw new Error('Route is already defined');
      }
    };

    this.unresolve = function(uri) {
      if (!routes.hasOwnProperty(uri)) {
        throw new Error('Route does not exist in collection');
      } else {
        delete routes[uri];
        return this;
      }
    };

    this.clearRoutes = function() {
      routes = {};
      return this;
    };

    this.createLink = function(uri) {
      var l = window.location;
      if (uri.match(/^(https?):\/\//)) {
        return uri;
      }
      if (uri.indexOf('/') > 0) {
        uri = Router.prototype.absolute(path, uri);
      }
      var port = (['80', '443'].indexOf(l.port) == -1 ? ':' + l.port : '');
      return `${l.protocol}//${l.hostname}${port}${uri}`;
    }
  }

  Router.prototype.getCurrentDirectory = function(path) {
    return Router.prototype.absolute(window.location.pathname, path);
  };

  Router.prototype.absolute = function (base, relative) {
    var stack = base.split("/"),
    parts = relative.split("/");
      stack.pop(); // remove current file name (or empty string)
      // (omit if "base" is the current folder without trailing slash)
      for (var i = 0; i < parts.length; i++) {
        if (parts[i] == ".") continue;
        if (parts[i] == "..") {
          stack.pop();
        } else {
          stack.push(parts[i]);
        }
      }
      return stack.join("/");
    };

    return Router;

}());
