/**
 * Collections.
 * This is a client-side JS collection handling system for data model responses.
 */
(new function() {
  return require(`${__dirname}/random.js`, function(Random) {
    return (function Collection(response, options) {
      let res = response;
      let opts = options;
      let self = this;

      const logEmpty = function() {
        console.warn('This collection is empty and cannot be called in this manner');
      };

      const ctype_digit = function(num) {
    		if (typeof(num) === 'string') {
    			return (!!num.match(/^([\d]+|[\d]+\.[\d]+)$/));
    		} else if (typeof(num) === 'number') {
    			return true;
    		} else {
    			return false;
    		}
    	};

      const requireInitialised = function() {
        if (!self.isInitialised()) {
          throw new Error('Collection is not initialised');
        }
      };

      const requireOK = function() {
        if (!self.isOK()) {
          throw new Error('Collection is empty');
        }
      };

      const _getSeed = function() {
        return Date.now() % (2 ** 64);
      };

      const _shuffle = function(array, seed) {
        var random = new Random(seed || _getSeed());
        // Using the Fisher-Yates shuffle and a pseudo-random
        // generator so that the random queue can be saved if wanted
        var m = array.length, t, i;

        // While there remain elements to shuffle…
        while (m) {

          // Pick a remaining element…
          i = Math.floor(random.nextFloat() * m--);

          // And swap it with the current element.
          t = array[m];
          array[m] = array[i];
          array[i] = t;
        }

        return array;
      };

      this.shuffler = _shuffle;

      // const _fakeCollection = function(opts) {
      //   return new Collection({
      //       status: {
      //         code: 200,
      //         message: 'OK',
      //         reason: null,
      //         data_count: 0,
      //         record_count: 0,
      //         page_count: 0,
      //         page: 0
      //       },
      //       data: null
      //     }, opts);
      // };

      this.createInstance = function(data, opts) {
        return new Collection(data, opts);
      };

      this.import = function(collection) {
        res = collection.getResponse();
        opts = collection.getOpts();
        behaviour = collection.getBehaviour();
      };

      this.isOK = function() {
        return !!(res && res.status && res.status.code === 200 && res.data);
      };

      this.isInitialised = function() {
        return !!(res && opts && opts.resource);
      };

      this.isFinished = function() {
        return res.status.page >= res.status.page_count;
      };

      this.next = function(prefs) {
        requireInitialised();
        if (this.isOK() && opts.referer.get) {
          // Return the next set of data in this collection
          var finished = res.status.page >= res.status.page_count;

          if (finished && !prefs.silent) {
            return Promise.reject(new Error('Reached the end of the collection'));
          } else if (finished && prefs.silent) {
            var fakedOpts = Object.assign({}, opts);
            fakedOpts.page++;
            var fakedData = this.getResponse();
            fakedData.status.page++;
            fakedData.status.data_count = 0;
            fakedData.data = null;
            return Promise.resolve(new Collection(fakedData, fakedOpts));
          }

          opts.page = parseInt(res.status.page) + 1;
          return opts.referer.get(opts);
        } else if (prefs.silent) {
          var fakedData = this.getResponse();
          fakedData.status.page = opts.page || 1;
          fakedData.status.data_count = 0;
          fakedData.data = null;
          return Promise.resolve(new Collection(fakedData, opts));
        } else {
          logEmpty();
          return Promise.reject(new Error('The next state cannot be retrieved due to the collection state'));
        }
      };

      this.previous = function(prefs) {
        requireInitialised();
        if (this.isOK() && opts.referer.get) {
          // Return the previous set of data in this collection
          var finished = res.status.page <= 0;

          if (finished && !prefs.silent) {
            return Promise.reject(new Error('Reached the start of the collection'));
          } else if (finished && prefs.silent) {
            var fakedOpts = Object.assign({}, opts);
            fakedOpts.page--;
            var fakedData = this.getResponse();
            fakedData.status.page++;
            fakedData.status.data_count = 0;
            fakedData.data = null;
            return Promise.resolve(new Collection(fakedData, fakedOpts));
          }

          opts.page = parseInt(res.status.page) - 1;
          return opts.referer.get(opts);
        } else if (prefs.silent) {
          var fakedData = this.getResponse();
          fakedData.status.page = opts.page || 1;
          fakedData.status.data_count = 0;
          fakedData.data = null;

          return Promise.resolve(new Collection(fakedData, opts));
        } else {
          logEmpty();
          return Promise.reject(new Error('The previous state cannot be retrieved due to the collection state'));
        }
      };

      this.find = function(search) {
        if (!search || (search && typeof(search) !== 'object')) {
          return Promise.reject();
        }

        // TODO find stuff until we reach the end
      };

      this.sort = function() {
        return res.data.sort.apply(res.data, arguments);
      };

      this.filter = function() {
        return res.data.filter.apply(res.data, arguments);
      }

      // Returns the current data context
      this.getData = function() {
        return res.data;
      };

      this.getResponse = function() {
        return res;
      };

      this.getOpts = function() {
        return Object.assign({}, opts);
      }

      this.getStatus = function() {
        return res.status;
      };

      this.getSeed = _getSeed;

      this.randomizeCollectionIds = function(seed) {
        // Returns a list of ID's sorted by the random (with seed). This method
        // fills an static array of numbers from 0 to the collections maximum
        // data count, and sorts them.
        requireOK();
        var list = new Array(res.status.record_count);

        for (var i = 0; i < res.status.record_count; i++) {
            list[i] = i;
        }

        return _shuffle(list, seed);
      };

      this.getCollectionIds = function() {
        var list = new Array(res.status.record_count);

        for (var i = 0; i < res.status.record_count; i++) {
            list[i] = i;
        }
        return list;
      };

      this.retrieveItemById = function(id) {
        if (!ctype_digit(id)) {
          return Promise.reject();
        }
        var _opts = Object.assign(opts, {query: {id: id}});
        return opts.referer.get(_opts);
      };

      this.clearAll = function() {
        status = null;
        res = null;
      };

      this.clearData = function() {
        res.data = 1;
      };

      this.retrieveItemsByIdList = function(list, batch_size) {
        requireOK();

        batch_size = (ctype_digit(batch_size) && batch_size >= 25 ? batch_size : 200);
        var promises = [];
        for (var i = 0; i < list.length; i += batch_size) {
          var _opts = Object.assign(opts, {limit: batch_size, query: {id: list.slice(i, i + batch_size)}, no_collection: true});
          promises.push(opts.referer.get(_opts));
        }

        return Promise.all(promises).then(function(res) {
          var data = [];
          var change = false;

          res.forEach(function(val, i) {
              if (val && val.data && val.data.length > 0) {
                data = data.concat(val.data);
              }
          });
          //console.log(data);

          return self.createInstance({
            status: {
              code: 200,
              message: 'OK',
              reason: null,
              page_count: 1,
              record_count: list.length,
              data_count: data.length,
              page: 1
            },
            data: data
          }, _opts);

        });
      };

      this.getAll = function(_opts) {
        var data = [];
        _opts = Object.assign({}, opts, _opts);
        _opts.page = 1;
        _opts.limit = 1000;
        _opts.no_collection = true;
        var handler = function(res) {
          data = data.concat(res.data);
          if (res.status.page < res.status.page_count) {
            _opts.page = ++res.status.page;
            return opts.referer.get(_opts).then(handler);
          } else {
            return self.createInstance({
              status: {
                code: 200,
                message: 'OK',
                reason: null,
                page_count: _opts.page,
                record_count: data.length,
                data_count: data.length,
                page: _opts.page
              },
              data: data
            }, _opts);
          }
        }
        return opts.referer.get(_opts).then(handler);
      };

    });
  }, true);

}());
