(function () {

  const cached = {};
  let base = '';

  const _fetch = function(filepath, cache) {
    cache = cache || false;
    // Only grab cached files if we allow it and when one has been cached
    if (cache && cached[filepath]) {
      return Promise.resolve(cached[filepath]);
    }
    return new Promise(function(resolve, reject) {
      var xhttp = new XMLHttpRequest();
      xhttp.overrideMimeType("text/plain");
      xhttp.onreadystatechange = function(res) {
        if (this.readyState === 4 && this.status === 200) {
          if (!cached[filepath]) {
            cached[filepath] = {responseText: this.responseText, status: 200};
          }
          resolve(this);
        } else if (this.readyState === 4 && this.status !== 200) {
          reject(this);
        }
      };
      xhttp.open("GET", filepath, true);
      xhttp.setRequestHeader("Content-Type", "text/plain");
      xhttp.send();
    });
  };

  _fetch.cache = cached;

  const fetch = function(filepath, impliedExtension, cache) {
    return _fetch(filepath, cache)
    .then(function(res) {
      var path = filepath + (impliedExtension || '');
      if (path.match(/\.(php|htm[l]?|txt)$/)) {
        return Promise.resolve(res.responseText);
      } else if (path.match(/\.(js)$/)) {
        try {
          var split = filepath.split('/');
          var __filename = split.pop();
          var __filepath = filepath;
          var __dirname = split.join('/');
          var fn = eval(res.responseText + '\n// File: ' + __filepath);
          // if (module && module.exports && Object.keys(module.exports) > 0) {
          //   return Promise.resolve(module.exports);
          // }
          return Promise.resolve(fn);
        } catch (e) {
          //console.error(e.stack);
          console.error(e);
          document.clear();
          document.write(e);
          console.error('An error occured trying to evalute the file:', __filepath, e);
        }
      } else if (path.match(/\.(json)$/)) {
        try {
          return Promise.resolve(JSON.parse(res.responseText));
        } catch (e) {
          return Promise.reject(e);
        }
      } else if (path.match(/http[s]?\:\/\//)) {
        return Promise.resolve(res.responseText);
      } else {
        debugger;
        return Promise.reject(new Error('Unable to require this resource due to an unknown file type for ' + filepath));
      }
    })
    .catch(function(res) {
      if (!res.status) {
        return Promise.reject(res);
      } else {
        return Promise.reject(new Error('Unable to require this resource due to ' + res.status + ' status code for ' + filepath));
      }
    });
  };

  const require = function(deps, cb, cache) {
    var check_deps = deps && typeof(deps) == 'object' && deps.constructor.name === 'Array';
    var check_dep = deps && typeof(deps) == 'string';
    var check_cb = (cb && typeof(cb) == 'function');
    if (check_deps) {
      deps = deps.concat();
      for (var dep in deps) {
        deps[dep] = fetch(base + deps[dep], cache);
      }
      return Promise.all(deps).then(function(fulfilled) {
        return (check_cb ? cb.apply(null, fulfilled) : fulfilled);
      });
    } else if (check_dep) {
      return fetch(deps).then(function(fulfilled) {
        return (check_cb ? cb.apply(null, [fulfilled]) : fulfilled);
      });
    } else {
      throw new Error('Invalid require parameters');
    }
  };

  require.setBasePath = function(path) {
    if (!path) {
      base = '';
    } else {
      base = path;
    }
  };

  const mount = function(filepath, variablename, obj, impliedExtension, cache) {
    variablename = variablename || filepath.replace(/^.*\//, '').replace(/\.(\w+)$/, '');
    if (typeof(variablename) !== 'string') {
      throw new Error('Invalid variable name for mount point');
    }
    obj = obj || this;
    return fetch(filepath, impliedExtension, cache).then(function(imported) {
      obj[variablename] = imported;
      return Promise.resolve(imported);
    });
  };

  window.fetch = fetch;
  window.mount = mount;

  window.require = require;

  return {fetch: fetch, require: require, mount: mount};

}());
