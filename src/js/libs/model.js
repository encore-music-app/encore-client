/**
 * Models.
 * This is a client-side JS model handling system.
 */
 (new function() {
  return require(`${__dirname}/collections.js`).then(function(Collection) {

    function Model(resource, child) {
      const events = {};
      const _resource = resource;

      const fire = function(event) {
        var args = Array.prototype.slice.apply(arguments, [1]);
        if (events.hasOwnProperty(event)) {
          var list = events[event];
          for (var index in list) {
            list[index].apply(list[index], args);
          };
        }
      };

      const on = function(event, callback) {
        if (events.hasOwnProperty(event)) {
          events[event].push(callback);
        } else {
          events[event] = [callback];
        }
        return this;
      };

      const off = function(event, callback) {
        if (event && events.hasOwnProperty(event)) {
          if (callback && (index = events[event][callback])) {
            delete events[event][index];
          } else {
            delete events[event];
          }
        } else if (!event) {
          events = {};
        }
        return this;
      };

      const xhr = function(opts) {
        opts = Object.assign({}, opts || {});

        opts.query = opts.query || {};
        opts.query.noprompt = 1;

        var query = [];
        for (var i in opts.query) {
          query.push(i + '=' + encodeURIComponent(opts.query[i]));
        }

        query = (query.length > 0 ? '?' + query.join('&') : '');

        var resource = (opts.resource && opts.resource !== '/' ? opts.resource + '/' + (opts.id ? opts.id : '') : '');
        var url = getAPIPath(`${resource}${query}`);

        if (!opts.method || (opts.method && typeof(opts.method) !== 'string')) {
          throw new Error('No valid method was supplied');
        }
        opts.method = opts.method.toUpperCase();
        opts.unpacker = opts.unpacker || JSON.parse;

        if ((opts.method === 'DELETE' || opts.method === 'PUT') && !opts.id) {
          throw new Error('An id is required for this API method');
        }

        return new Promise(function(resolve, reject) {

          var xhr = new XMLHttpRequest();

          xhr.onreadystatechange = function() {
            //console.log(this);
            if ((this.readyState === 4 && this.status === 200) || (this.readyState === 4 && opts.silent)) {

              var obj = opts.unpacker(this.responseText);
              if (!(opts.no_collection === true)) {
                obj = new Collection(obj, opts);
              }
              resolve(obj);

            } else if (this.readyState === 4 && this.status === 404) {

              if (this.responseText && (data = opts.unpacker(this.responseText))) {
                if (!(opts.no_collection === true)) {
                  data = new Collection(data, opts);
                }
                reject(data);
              } else {
                if (opts.id !== '') {
                  reject(new Error(`Item doesn't exist in ${opts.resource} collection`));
                } else {
                  reject(new Error(`Collection '${opts.resource}' doesn't exist`));
                }
              }

            } else if (this.readyState === 4 && this.status !== 200) {

              if (this.responseText && (data = opts.unpacker(this.responseText))) {
                if (!opts.no_collection) {
                  data = new Collection(data, opts);
                }
                reject(data);
              } else if (this.status === 401 && !opts.auth) {
                fire('unauthorized', this, opts);
                reject(new Error('unauthorized'));
              } else {
                reject(this); //new Error('Unsupported response'));
              }
            }

          };

          xhr.open(opts.method, url, true);

          if (opts.auth && opts.auth.username && opts.auth.password) {
            var auth = btoa(`${opts.auth.username}:${opts.auth.password}`);
            xhr.setRequestHeader("Authorization", `Basic ${auth}`);
          }

          if (opts.data && (opts.method !== 'GET' && opts.method !== 'DELETE')) {
            var form = new FormData();
            for (var key in opts.data) {
              if (opts.data.hasOwnProperty(key)) {
                form.append(key, opts.data[key]);
              }
            }
            opts.data = form;
          } else {
            opts.data = null;
          }

          xhr.send(opts.data);
        });
      };

      const isIdentifier = function(arg) {
        return (
          arg !== 'undefined' &&
          arg !== null &&
          typeof(arg) === 'string' &&
          arg !== '' &&
          arg.match(/[\w\d_]+/)
          );
      };

      const getAPIPath = function(resource) {
        resource = (resource && typeof(resource) == 'string' ? resource : '');
        return `${__rootdir}/api/v1/${resource}`;
      };

      this.on = on;
      this.off = off;

      this.get = function(opts) {
        opts = /*Object.assign({},*/ opts || {}/*)*/;

        opts.resource = opts.resource || '';

        opts.id = (opts.id ? opts.id : '');

        opts.query = Object.assign({}, opts.query || {});

        opts.limit = (opts.limit && !isNaN(opts.limit) ? opts.limit : '');
        opts.page = (opts.limit && !isNaN(opts.page) ? opts.page : '');

        if (opts.limit) {
          opts.query.limit = opts.limit;
        }
        if (opts.page) {
          opts.query.page = opts.page;
        }

        opts.method = opts.method || 'GET';
        if (['GET', 'OPTIONS', 'HEAD'].indexOf(opts.method.toUpperCase()) == -1) {
          console.warn('Method ' + opts.method.toUpperCase() + ' is not allowed in Model.get');
          opts.method = 'GET';
        }
        opts.method = opts.method;
        opts.referer = this;

        return xhr(opts);
      };

      this.delete = function(opts) {
        opts = /*Object.assign({},*/ opts || {}/*)*/;

        if (!isIdentifier(opts.id)) {
          return Promise.reject(new Error('Cannot delete a resource without an id'));
        }
        opts.id = ((opts.id || null) ? opts.id : '');

        opts.method = 'DELETE';
        opts.referer = this;

        return xhr(opts);
      };

      this.update = function(opts) {
        opts = /*Object.assign({},*/ opts || {}/*)*/;

        if (!isIdentifier(opts.id)) {
          return Promise.reject(new Error('Cannot update a resource without an id'));
        } else if (!opts.data) {
          return Promise.reject(new Error('No data to update on resource!'));
        }
        opts.id = ((opts.id || null) ? opts.id : '');

        opts.method = 'PUT';
        opts.referer = this;

        return xhr(opts);
      };

      this.create = function(opts) {
        opts = /*Object.assign({},*/ opts || {}/*)*/;

        if (!opts.data) {
          return Promise.reject(new Error('No data to create a resource!'));
        }
        //opts.id = ((opts.id || null) ? `/${opts.id}` : '');
        opts.method = 'POST';
        opts.referer = this;

        return xhr(opts);
      };

      this.getResource = function() {
        return _resource;
      };

      this.getPath = getAPIPath;

      // For each get function, prepare some arguments
      const handle = {
        get: function(object, prop, receiver) {
          if (prop in object && typeof(object[prop]) === 'function') {
            return function(opts) {
              opts = opts || {};
              opts.resource = opts.resource || _resource;
              opts.model = object;
              return object[prop](opts);
            };
          } else if (prop in object) {
            return object[prop];
          } else {
            return undefined;
          }
        },
      };

      let parent = new Proxy(this, handle);

      // By default allow the Model class to be simply extended easily
      const extend = function(parent, child) {
        var handler = {
          get: function(obj, prop, receiver) {
            if (prop === 'parent') {
              return parent;
            } else if (prop in child) {
              return child[prop];
            } else if (prop in parent) {
              return parent[prop];
            }
          },
          set: function(obj, prop, value) {
            child[prop] = value;
          }
        };

        return new Proxy({}, handler);
      };

      if (child !== undefined && child !== null &&
        (typeof(child) === 'object' || typeof(child) === 'function')) {
          parent = extend(parent, child);
      }

      return parent;
    };

    Model.prototype.Collection = Collection;

    return Model;
  }, true);
}());
