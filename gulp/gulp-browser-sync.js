module.exports = function (gulp, browserSync, conf, errorHandler) {
  gulp.task(
    'browser-sync',
    function (done) {
      browserSync.init(
        {
          server: {
            baseDir: conf.serve.base,
            port:    conf.serve.port
          }
        }
      );

      done();
    }
  );
};
