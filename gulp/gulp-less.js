var less = require('gulp-less');
var path = require('path');
var sourcemaps = require('gulp-sourcemaps');

module.exports = function (gulp, plugins, conf, errorHandler) {

  gulp.task('less', function () {
    return gulp.src(conf.gulp.less.src)
      .pipe(plugins.sourcemaps.init())
      .pipe(less({
        paths: conf.gulp.less.src
      }))
      .pipe(plugins.sourcemaps.write())
      .pipe(gulp.dest(conf.gulp.less.dist));
  });

};
