var path = require('path');

module.exports = function (gulp, plugins, conf, errorHandler) {
  gulp.task(
    'font',
    function (done) {
      // For now we don't need anything very advanced.
      gulp.src(conf.gulp.font.src).pipe(gulp.dest(conf.gulp.font.dist));
      done();
    }
  );
};
