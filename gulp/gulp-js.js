var rename = require('gulp-rename');
var fs = require('fs');

module.exports = function (gulp, plugins, conf, errorHandler) {
  gulp.task(
    'js',
    function (done) {

      // gulp.src(
      //   conf.gulp.js.concat.files, {allowEmpty: true}
      // )
      // .pipe(plugins.concat('libraries.min.js'))
      // .pipe(plugins.sourcemaps.init())
      // .pipe(plugins.sourcemaps.write())
      // .pipe(gulp.dest(conf.gulp.js.concat.dist));

      gulp.src(
        conf.gulp.js.copy.src, {allowEmpty: true}
      ).pipe(rename(function(path) {
        //console.log(path);
        // This could probably be done a better way!
        // var file = path.dirname + '/' + path.basename + path.extname;
        // Copy all JS resources manually to resources/js under:
        // {kind}/{kind_name}/{js_filename}
        path.dirname = path.dirname.replace(/\/_?(resources)\/(js)$/, '').toLowerCase();
      })).pipe(gulp.dest(conf.gulp.js.copy.dist));

      done();
    }
  );
};
