var path = require('path');
var fs = require('fs');


module.exports = function (gulp, plugins, conf, errorHandler) {
  gulp.task(
    'index',
    function (done) {

      var dist = path.resolve(`${__dirname}/../${conf.assets.dist}`);

      try {
        if (!fs.existsSync(dist)) {
          fs.mkdirSync(dist);
        }
      } catch (e) {
        throw new Error(`Unable to create distribution folder ${dist}`);
      }

      gulp.src(conf.serve.index.replace(/(\/\w+.\w+)$/, '') + '/favicon.ico')
      .pipe(gulp.dest(conf.assets.dist));

      gulp.src(conf.serve.index).pipe(gulp.dest(conf.assets.dist));

      var directories = ['css', 'js', 'images', 'fonts'];
      for(var index in directories) {
        var dirname = directories[index];

        var dir = path.resolve(`${dist}/${dirname}`);

        try {
          if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
          }
        } catch (e) {
          throw new Error(`Unable to create distribution folder ${dir}`);
        }
      }

      done();
    }
  );
};
