module.exports = function (gulp, browserSync, conf, errorHandler) {
  gulp.task(
    'browser-sync-proxy',
    function (done) {
      browserSync.init(
        {
          proxy: "localhost:" + conf.serve.port,
          port:  conf.serve.proxyPort,
          index: conf.serve.index
        }
      );

      done();
    }
  );
};
