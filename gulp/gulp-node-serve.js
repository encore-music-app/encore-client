module.exports = function (gulp, exec, conf, errorHandler) {
  gulp.task(
    "node-serve",
    function (done) {
      let child = exec("node bin/www");

      child.stdout.on(
        'data',
        function (data) { console.log(data); }
      );

      child.stderr.on(
        'data',
        function (data) { console.log(data); }
      );

      child.on(
        'close',
        function (code) { console.log('Closing code: ' + code); }
      );

      done();
    }
  );
};
