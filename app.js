var debug = require('debug')('web-player:client');
express = require('express');
var fs = require('fs');
//var request = require('request');
var proxy = require('express-http-proxy');
var app = express();
var index = require('./src/routes/index');

// Never show that Express/Node.js is used
app.disable('x-powered-by');

// view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'pug');

var yaml = require('js-yaml');
var conf = yaml.load(fs.readFileSync('./gulp/config.yml', 'utf8'));
var api_server = conf.api_server;
var api_path = (
	//(api_server.secure ? 'https://' : 'http://') +
	api_server.host +
	(api_server.port ? `:${api_server.port}` : '')
);

// Redirect API requests to the server
app.use('/api', proxy(api_path, {
	proxyReqPathResolver: function(req) {
		return Promise.resolve(req.originalUrl);
	},
}));

app.use(index);

// error handler
app.use(function(err, req, res, next) {
	// Temporary error handler which is much clearer for now!
	if (err.status === 404) {
		res.send('404 - Not Found');
	} else {
		console.error('Error! ' + err + '\n' + err.stack);
		if (req.app.get('env') === 'development') {
			res.send('<h1>A server error occured!</h1><br/><p><b>' + err + '</b></p><span>Stack Trace:</span><br/>' + err.stack); // .replace(/\n/g, '<br/>')
		} else {
			res.send('500 - Internal Server Error');
		}
	}
});

module.exports = app;
